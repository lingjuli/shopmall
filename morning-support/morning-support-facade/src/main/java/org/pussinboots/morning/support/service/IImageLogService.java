package org.pussinboots.morning.support.service;

import org.pussinboots.morning.support.entity.ImageLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-support-facade   
* 类名称：IImageLogService   
* 类描述：IImageLogService / 云存储图片记录表 业务逻辑层接口
* 创建人：代远航
* 创建时间：2019年7月11日 下午8:14:46
*
 */
public interface IImageLogService extends IService<ImageLog> {
	
}
