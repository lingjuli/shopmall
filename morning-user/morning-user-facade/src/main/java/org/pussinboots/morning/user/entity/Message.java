package org.pussinboots.morning.user.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 通知消息表
 * </p>
 *
 * @author ${author}
 * @since 2019-11-15
 */
@TableName("os_message")
public class Message extends Model<Message> {

    private static final long serialVersionUID = 1L;

    /**
     * 信息ID
     */
	@TableId(value="message_id", type= IdType.AUTO)
	private Long messageId;
    /**
     * 收信人ID
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 收信人昵称
     */
	@TableField("user_name")
	private String userName;
    /**
     * 信息内容
     */
	private String content;
    /**
     * 信息类型：1交易物流  2通知消息 3特惠活动 6意见反馈
     */
	private Integer type;
    /**
     * 阅读状态：1.已读；0.未读
     */
	private Integer status;
    /**
     * 发信人ID
     */
	@TableField("send_user_id")
	private Long sendUserId;
    /**
     * 发信人昵称
     */
	@TableField("send_user_name")
	private String sendUserName;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;


	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getSendUserId() {
		return sendUserId;
	}

	public void setSendUserId(Long sendUserId) {
		this.sendUserId = sendUserId;
	}

	public String getSendUserName() {
		return sendUserName;
	}

	public void setSendUserName(String sendUserName) {
		this.sendUserName = sendUserName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.messageId;
	}

}
