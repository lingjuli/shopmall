package org.pussinboots.morning.user.service;

import org.pussinboots.morning.user.entity.UserLoginLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-user-facade   
* 类名称：IUserLoginLogService   
* 类描述：UserLoginLog / 用户登录表 业务逻辑层接口    
* 创建人：代远航
* 创建时间：2019年4月8日 下午2:14:00
*
 */
public interface IUserLoginLogService extends IService<UserLoginLog> {
	
}
