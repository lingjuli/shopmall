package org.pussinboots.morning.user.pojo.vo;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;
import java.util.Date;

public class UserVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
    /**
     * 用户ID
     */
	private Long userId;
	
    /**
     * 用户编号
     */
	private Long userNumber;
	
    /**
     * 昵称
     */
	private String userName;
	
    /**
     * 真实姓名
     */
	private String realName;
	
    /**
     * 性别 0=保密/1=男/2=女
     */
	private Integer sex;
	
    /**
     * 年龄
     */
	private Integer age;
	
    /**
     * 用户头像
     */
	private String picImg;
	
    /**
     * 电子邮箱
     */
	private String email;
	
    /**
     * 手机号码
     */
	private String telephone;
	/**
	 * 注册时间
	 */
	private Date regeistTime;

	/**
	 * 会员积分
	 */
	private Integer score;

	/**
	 * 省份ID
	 */
	private Integer provinceId;
	/**
	 * 省份名字
	 */
	private String provinceName;

	/**
	 * 城市ID
	 */
	private Integer cityId;
	/**
	 * 城市名字
	 */
	private String cityName;


	private String sexStr;
	private String ageStr;
	private String place;

	public String getSexStr() {
		return sexStr;
	}

	public void setSexStr(String sexStr) {
		this.sexStr = sexStr;
	}

	public String getAgeStr() {
		return ageStr;
	}

	public void setAgeStr(String ageStr) {
		this.ageStr = ageStr;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getRegeistTime() {
		return regeistTime;
	}

	public void setRegeistTime(Date regeistTime) {
		this.regeistTime = regeistTime;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}



	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(Long userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPicImg() {
		return picImg;
	}

	public void setPicImg(String picImg) {
		this.picImg = picImg;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
