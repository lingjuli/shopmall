package org.pussinboots.morning.user.service;


import com.baomidou.mybatisplus.service.IService;
import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.user.entity.Message;

/**
 * <p>
 * 通知消息表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2019-11-15
 */
public interface IMessageService extends IService<Message> {
    //获取消息列表（分页）
    BasePageDTO<Message> getMessagelist(Long userId, PageInfo pageInfo);
    //获取消息列表（总数）
    Integer getMessageCount(Long userId);

	
}
