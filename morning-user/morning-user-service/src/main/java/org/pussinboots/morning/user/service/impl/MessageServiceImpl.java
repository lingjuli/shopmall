package org.pussinboots.morning.user.service.impl;

import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.user.entity.Message;
import org.pussinboots.morning.user.mapper.MessageMapper;
import org.pussinboots.morning.user.service.IMessageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 通知消息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2019-11-15
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {
    @Autowired
    private MessageMapper messageMapper;
    @Override
    public BasePageDTO<Message> getMessagelist(Long userId, PageInfo pageInfo) {
        pageInfo.setTotal(messageMapper.getCount(userId));
         List<Message> messagelist=messageMapper.getList(userId,pageInfo);
        BasePageDTO<Message> vBasePageDTO=new BasePageDTO(pageInfo,messagelist);
        return vBasePageDTO;
    }

    @Override
    public Integer getMessageCount(Long userId) {
        return messageMapper.getCount(userId);
    }


}
