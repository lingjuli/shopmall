package org.pussinboots.morning.user.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.user.entity.Message;

import java.util.List;

/**
 * <p>
  * 通知消息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2019-11-15
 */
public interface MessageMapper extends BaseMapper<Message> {

    int getCount(@Param("userId")Long userId);

    List<Message> getList(@Param("userId")Long userId, @Param("pageInfo")PageInfo pageInfo);

}