package org.pussinboots.morning.user.mapper;

import org.pussinboots.morning.user.entity.Favorit;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-user-service   
* 类名称：FavoriteMapper   
* 类描述：Favorite / 收藏夹表 数据访问层接口   
* 创建人：代远航
* 创建时间：2019年5月9日 下午7:57:48
*
 */
public interface FavoritMapper extends BaseMapper<Favorit> {

}