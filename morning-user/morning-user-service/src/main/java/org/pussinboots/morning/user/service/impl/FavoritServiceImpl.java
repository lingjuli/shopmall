package org.pussinboots.morning.user.service.impl;

import java.util.List;

import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.user.entity.Favorit;
import org.pussinboots.morning.user.mapper.FavoritMapper;
import org.pussinboots.morning.user.service.IFavoritService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/**
 * 
* 项目名称：morning-user-service   
* 类名称：FavoriteServiceImpl   
* 类描述：Favorite / 收藏夹 业务逻辑层接口实现    
* 创建人：代远航
* 创建时间：2019年5月9日 下午7:58:20
*
 */
@Service
public class FavoritServiceImpl extends ServiceImpl<FavoritMapper, Favorit> implements IFavoritService {
	
	@Autowired
	private FavoritMapper favoritMappper;

	@Override
	public BasePageDTO<Favorit> listByUserId(Long userId, PageInfo pageInfo) {

		Page<Favorit> page = new Page<>(pageInfo.getCurrent(), pageInfo.getLimit());

		Favorit favorite = new Favorit();
		favorite.setUserId(userId);
		favorite.setStatus(1);
		List<Favorit> favorites = favoritMappper.selectPage(page,
				new EntityWrapper<Favorit>(favorite).orderBy("createTime", false));
		pageInfo.setTotal(page.getTotal());

		return new BasePageDTO<Favorit>(pageInfo, favorites);
	}

	@Override
	public Integer deleteByProductNumber(Long userId, Long productNumber) {
		Favorit sfavorite = new Favorit();
		sfavorite.setUserId(userId);
		sfavorite.setProductNumber(productNumber);
		Favorit favorit=favoritMappper.selectOne(sfavorite);
		favorit.setStatus(2);
		return favoritMappper.updateById(favorit);
	}

	@Override
	public Integer selectCount(Favorit favorit) {
		return favoritMappper.selectCount(new EntityWrapper<Favorit>(favorit));
	}
}
