# JAVA技术B2C购物网站系统

#### 介绍
萌发B2C购物网站系统，B2C模式的购物网站系统。采用主流java技术开发，设计理念先进，功能强大，代码结构清晰，可直接部署进行商业运营。
1.B2C模式的购物网站系统；
2.先进的JAVA Web技术架构实现，有海量的java优异后备技术选项，可无限扩充系统的性能边界；
3.优秀的购物网站前端设计，美观、简洁、易用，超越市面上大部分电商购物系统；
4.完整的积分管理功能；
5.主流的网上支付技术集成，系统已集成了支付宝支付和微信支付，只需要配置参数即可实现网站的在线支付完成购物。
6.独立自主可控理念。除了必要的第三方支付以外，系统在用户注册、链接分享、图片管理上均不依赖第三方，做到网站功能自主可控，尽可能实现网站不受到第三方限制，提升网站的运营稳定性。
7.以产品为中心的运营理念，为你的成功电商运营提供了有效支撑。其中的技术包含：通过配置，产品可以出现在首页分类中、产品在首页置顶显示、产品在首先热门显示；还可以配置产品出现在新品推荐、热评产品、为你推荐、明星产品 等区域，为你的运营提供了多种可能性，实现更优化的网站运营效果。
8.结构清晰，代码工整，可直接进行部署运营。如有个性化需求，可根据需求在现有系统基础上进行二次开发得到自己想要的系统；
9.主体架构：Spring + Spring MVC + MyBatis + Bootstrap + Tomcat + Mysql数据库。

#### 软件架构
软件架构说明
1、后端
核心框架：Spring Framework 4.3.5
安全框架：Apache Shiro 1.3.2
视图框架：Spring MVC 4.3.5
任务调度：Spring + Quartz 2.2.3
持久层框架：MyBatis 3.4.2 + Mybatis-plus 2.0.1
数据库连接池：Alibaba Druid 1.0
缓存框架：Ehcache 2.6 + Redis 2.9.0
日志管理：SLF4J 1.7 + Log4j2 2.7
布局框架：SiteMesh 3.0.1
分布式应用程序协调服务：ZooKeeper 3.3.1
分布式服务框架：Dubbo 2.5.3
接口测试框架：Swagger2 2.6.1
工具类：Apache Commons、Jackson 2.2、fastjson 1.2.20
2、前端
JS框架：Jquery
表格插件：Bootstrap Table
表单验证插件：BootstrapValidator
日期选择插件：Datepicker for Bootstrap
弹层组件：Layer
数据图表：Echarts
表单美化插件：ICheck
树形视图插件：Ztree

#### 安装教程

1、具备运行环境：JDK1.7+、Maven3.0+、MySql5+；
2、创建数据库，数据库脚本位置在 project-doc\database\shopmall.sql，数据库名称为shopmall；
3、根据数据库参数，在如下文件中配置数据库参数：
 morning-common\src\main\resources\properties\jdbc.properties 
 morning-os-web\src\main\resources\profiles\dev\jdbc.properties

运行方式一：在IDEA中运行：
   1.直接在IDEA中运行tomcat-os；
   2.根据idea中tomcat的端口配置访问系统，比如：http://127.0.0.1:8080/



运行方式二：打成war包在tomcat中运行,比如项目打成shopmall.war,这时需要做如下适配:

1.修正系统中商品详情图片的路径，在网页图片路径中加项目部署名称shopmall：
在morning-os-web\.....\ProductDetailController.java/item方法中，加入如下代码即可：
String oldimgpath="/uploads/images/goods/";
String newimgpath="/shopmall/uploads/images/goods/";  //显示系统的网页图片路径修正
String productdetail=product.getDescription();
if(productdetail!=null && !"".equals(productdetail)){
	productdetail=productdetail.replaceAll(oldimgpath,newimgpath);
}

2.修改系统数据表中配置的商品连接地址，加上shopmall项目路径：
在数据库的os_navigation_bar表中，将商品连接地址加上shopmall项目路径；
比如http://localhost:8080/detail/4263114237484  修正为 http://localhost:8080/shopmall/detail/4263114237484 

3.通过IDEA中的Maven打包命令工具对整个项目进行打包；

4.打包完成后，得到shopmall.war文件。shopmall.war文件在shopmall\morning-os-web\target目录下。

5. 将 shopmall.war 放入专门的tomcat应用目录webapps，启动tomat；

6.根据专门的tomcat的端口配置访问系统，比如  http://127.0.0.1:8080/shopmall

#### 系统截图
1首页：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/093803_feeea236_7893215.jpeg "在这里输入图片标题")


2商品列表

![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/093815_4bfc7b4b_7893215.jpeg "在这里输入图片标题")

3商品详情：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/093908_25fe87c7_7893215.jpeg "在这里输入图片标题")

4购物车
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094030_ea430db6_7893215.jpeg "在这里输入图片标题")


5填写订单信息页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094036_0cb5f18b_7893215.jpeg "在这里输入图片标题")


6支付订单页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094109_d0352744_7893215.jpeg "在这里输入图片标题")


7个人中心主页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094118_d994435c_7893215.jpeg "在这里输入图片标题")


8订单列表
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094150_2bf4fd2a_7893215.jpeg "在这里输入图片标题")


9评价晒单

![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094155_6bb09857_7893215.jpeg "在这里输入图片标题")

10退货服务单页：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094229_62c84252_7893215.jpeg "在这里输入图片标题")


11收藏商品页：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/094235_afc76d29_7893215.jpeg "在这里输入图片标题")


12收货地址：
![输入图片说明](https://oscimg.oschina.net/oscnet/up-2a337331840753b04da633883ad1cd6716b.JPEG "在这里输入图片标题")



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
