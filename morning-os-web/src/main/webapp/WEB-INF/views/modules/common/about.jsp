<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>关于我们 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/order-confirm.css">
</head>
<body>
<!-- 轮播top菜单导航引入 -->
<jsp:include page="/WEB-INF/views/modules/common/site_header.jsp" />
<!-- 轮播top菜单导航引入 -->

<div class="page-main page-mini-main">
  <div class="container-fluid confirm-box">

      <div class="section section-order">
        <div class="order-info clearfix">
          <div class="fl">
            <h2 class="title">关于我们：</h2>
            <p class="order-time" id="J_deliverDesc">XXXXX购物商城由XXXXX公司创建，于2020年3月开始上线运营；</p>
            <p class="order-time">XXXXX公司成立于2018年，是一家专门为消费者提供优质商品的公司；</p>
            <p class="order-time">XXXXX公司成立于2018年，是一家专门为消费者提供优质商品的公司；</p>
            <p class="order-time">XXXXX公司成立于2018年，是一家专门为消费者提供优质商品的公司；</p>
            <p class="order-time">XXXXX公司成立于2018年，是一家专门为消费者提供优质商品的公司；</p>
            <p class="order-time">公司地址：浙江省杭州市西湖区文三路XXX号</p>
            <p class="order-time">联系电话：400-800-XXXX</p>
            <p class="order-time">电子邮箱：xxxxx@shop.com</p>
          </div>
        </div>
      </div>

  </div>
</div>


<myfooter> 
  <!-- layer javascript -->
  <script src="${ctxsta}/common/layer/layer.js"></script>

</myfooter>
</body>
</html>