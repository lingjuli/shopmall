<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>购物指南 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/order-confirm.css">
</head>
<body>
<!-- 轮播top菜单导航引入 -->
<jsp:include page="/WEB-INF/views/modules/common/site_header.jsp" />
<!-- 轮播top菜单导航引入 -->

<div class="page-main page-mini-main">
  <div class="container-fluid confirm-box">

      <div class="section section-order">
        <div class="order-info clearfix">
          <div class="fl">
            <h2 class="title">购物指南：</h2>
            <p class="order-time">用户如何注册？</p>
            <p class="order-time">如何购买商品，购买商品的流程？</p>
            <p class="order-time">如何下单？</p>
            <p class="order-time">如何修改订单？</p>
            <p class="order-time">如何取消订单？</p>
            <p class="order-time">如何支付订单？</p>
            <p class="order-time">如何评价订单？</p>
          </div>
        </div>
      </div>

  </div>
</div>


<myfooter> 
  <!-- layer javascript -->
  <script src="${ctxsta}/common/layer/layer.js"></script>

</myfooter>
</body>
</html>