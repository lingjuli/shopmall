<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>退货申请 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/address.css">
<link rel="stylesheet" href="${ctxsta}/os/area/css/select2.css" />
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-view-box">
      <div class="box-hd">
        <h1 class="title"><span class="text">退货申请</span></h1>
        <div class="more clearfix">
          <h2 class="subtitle">订单号：${orderVO.orderNumber} <span class="tag tag-subsidy"></span> </h2>
          <div class="actions">
           </div>
        </div>
      </div>
      <div class="box-bd">
        <div class="uc-order-item uc-order-item-finish">
          <div class="order-detail">
            <div class="order-summary">
            </div>
            <table class="order-items-table">
              <tbody>
                <c:forEach items="${orderVO.orderProducts}" var="orderProduct">
                  <tr>
                    <td class="checkbox" valign="middle"><input id="returnIds" value="${orderProduct.orderProductId}"  class="check-one check" type="checkbox"/></td>
                    <td class="col col-thumb"><div class="figure figure-thumb"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}" data-stat-id="${orderProduct.productNumber}"> <img src="${ctximg}/${orderProduct.picImg}" width="80" height="80" alt="${orderProduct.name}"> </a> </div></td>
                    <td class="col col-name"><p class="name"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}" data-stat-id="${orderProduct.productNumber}">${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}</a> </p></td>
                    <td class="col col-price"><p class="price">${orderProduct.price}元 × ${orderProduct.buyNumber}</p></td>
                    <td class="col col-actions"></td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="box-bd">
        <div class="uc-order-item uc-order-item-finish">
          <div id="editAddr" class="order-detail-info">
            <table class="info-table">
              <tbody>
              <tr>
                <th>退货原因：</th>
                <td>
                  <select  name="returnReason" id="returnReason"
                           class="form-control chosen-select" tabindex="2">
                    <c:forEach items="${returnReasons}" var="rr">
                    <option value="${rr.status}">${rr.stateInfo}</option>
                    </c:forEach>
                  </select>
                </td>
              </tr>
              <tr>
                <th>问题描述：</th>
                <td>
                  <input type="text" name="questionContent" maxlength="30" class="form-control "  id="questionContent" value="" />
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="more clearfix">
          <div class="actions">
            <a id="J_returnCommit" class="btn btn-small btn-primary" title="提交申请" href="javascript:returnCommit();"  data-order-id="${orderVO.orderId}" data-order-number="${orderVO.orderNumber}">提交申请</a>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
<div class="modal-backdrop in" style="display: none; width: 100%; height: 1854px;"></div>
<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script> 
  <script src="${ctxsta}/os/area/js/area.js"></script> 
  <script src="${ctxsta}/os/area/js/location.js"></script> 
  <script src="${ctxsta}/os/area/js/select2.js"></script> 
  <script src="${ctxsta}/os/area/js/select2_locale_zh-CN.js"></script> 
  <script src="${ctxsta}/os/js/order.js"></script>

  <script >
      function  returnCommit()   {
              var selectnum=$("input[type='checkbox']:checked").length;
              var showmsq="";
              if(selectnum<1){
                  showmsq="请选择需要退货的商品.";
              }
              var ids="";
              $.each($('input:checkbox:checked'),function(){
                  ids=ids+$(this).val()+",";
              });
              var orderId = $('#J_returnCommit').attr('data-order-id');
              var orderNumber = $('#J_returnCommit').attr('data-order-number');
              var returnReason=$('#returnReason').val();
              var questionContent=$('#questionContent').val();
              if(questionContent==""){
                  if(showmsq==""){
                      showmsq="请填写问题描述.";
                  }
              }
              if(showmsq.length>0){
                  layer.alert(showmsq);
              }else{
                  layer.confirm('你确定要提交退货申请吗?', {
                      btn : [ '确定', '取消' ] //按钮
                  }, function() {
                      $.ajax({
                          url : baselocation + '/uc/order/returnProductCommit',
                          type : 'POST',
                          dataType : 'json',
                          data : {
                              "orderId":orderId,
                              "orderNumber":orderNumber,
                              "orderProductIds":ids,
                              "returnReason":returnReason,
                              "questionContent":questionContent,
                          },
                          success : function(result) {
                              if (result.code == 1) {
                                  layer.alert("退货申请已提交", {
                                      icon : 2
                                  }, function() {
                                      location.href=baselocation + '/uc/order/'+orderNumber;
                                  });
                              } else {
                                  layer.alert(result.message, {
                                      icon : 2
                                  });
                              }
                          }
                      });
                  });
              }

          }

  </script>
</myfooter>
</body>
</html>