<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <title>意见反馈 -</title>
  <link rel="stylesheet" href="${ctxsta}/os/css/address.css">
  <link rel="stylesheet" href="${ctxsta}/os/area/css/select2.css" />
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-view-box">
      <div class="box-hd">
        <h1 class="title"><span class="text">意见反馈</span></h1>
        <div class="more clearfix">
        </div>
      </div>
      <div class="box-bd">
        <div class="uc-order-item uc-order-item-finish">
          <div id="editAddr" class="order-detail-info">
            <table class="info-table">
              <tbody>
              <tr >
                <td colspan="2">感谢您给我们提出宝贵建议，我们将认真听取并采纳，谢谢！</td>
              </tr>

              <tr >
                <th style="width: 120px;" >意见内容：</th>
                <td><textarea class="form-control" id="content" name="content" rows="5" cols="100"  placeholder="请输入意见内容"></textarea></td>
              </tr>

              <tr>
                <th>输入验证码：</th>
                <td>
                  <input type="text" name="randCode" maxlength="4" class="form-control required "  id="randCode" />
                  <label class="f-size12 c-999 f-fl f-pl10"> <img src="${ctx}/pass/captcha-image.jpg" id="kaptchaImage" /> </label>
                  <label class="focusa">看不清？<a href="javascript:changeCaptcha();" class="c-blue" onclick="">换一张</a></label>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="more clearfix">
          <div class="actions">
            <a id="J_payOrder" class="btn btn-small btn-primary"  href="javascript:saveSuggest();">提交</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<myfooter>
  <!-- layer javascript -->
  <script src="${ctxsta}/common/layer/layer.js"></script>
  <script>

      /**
       * 验证码更改
       */
      $(function() {
          $('#kaptchaImage').click(function() {
              $(this).attr('src', baselocation + '/pass/captcha-image.jpg?' + Math.floor(Math.random() * 100));
          })
      });

     // $("#kaptchaImage").trigger("click");

      function changeCaptcha(){
          $("#kaptchaImage").trigger("click");
      }

      function saveSuggest(){
          var content=$("#content").val();
          var randCode=$("#randCode").val();
          if(!isNonEmpty(content)){
              $("#content").focus();
              layer.alert("意见内容不能为空", {icon : 2});
              return;
          }

          if(!isNonEmpty(randCode)){
              $("#randCode").focus();
              layer.alert("验证码不能为空", {icon : 2});
              return;
          }

          $.ajax({
              type : "POST",
              url : baselocation + '/uc/user/saveSuggest',
              data : {
                  "content" : content,
                  "randCode" : randCode
              },
              dataType : "json",
              success : function(result) {
                  if (result.code == 1) {
                      //layer.alert("保存成功", {icon : 1});
                      //window.location.href = baselocation + '/uc/user/userMess';

                      layer.alert("提交意见成功", {
                          icon : 1,closeBtn: 0
                      }, function(){
                          window.location.href = baselocation + '/uc/user/portal';
                      });
                  } else {
                      layer.alert(result.message, {icon : 2});
                  }
              }
          })

      }


      var rule = { //验证规则
          phone : /^1[2|3|4|5|6|7|8|9]\d{9}$/,
          mail : /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
          company : /^[\u4E00-\u9FA5a-zA-Z][\u4E00-\u9FA5a-zA-Z0-9\s-,-.]*$/,
          uname : /^[\u4E00-\u9FA5a-zA-Z][\u4E00-\u9FA5a-zA-Z0-9_]*$/,
          zh : /^[\u4e00-\u9fa5]+$/, //纯中文
          card : /^((1[1-5])|(2[1-3])|(3[1-7])|(4[1-6])|(5[0-4])|(6[1-5])|71|(8[12])|91)\d{4}(((((19|20)((\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(\d{2}(0[13578]|1[02])31)|(\d{2}02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[48])0229)))|20000229)\d{3}(\d|X|x))|(((\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(\d{2}(0[13578]|1[02])31)|(\d{2}02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[48])0229))\d{3}))$/, //身份证号
          int : /^[0-9]*$/,
          s : ''
      };

      function isNonEmpty(value) { //不能为空
          if (value.length) return true; else return false ;
      }
      function numBetween(value,range) { //大于小于
          var min = parseInt(range.split('-')[0]);
          var max = parseInt(range.split('-')[1]);
          if (value >= min && value <= max) return true; else return false ;
      }
      function lengthBetween(value,range) { //大于小于
          var min = parseInt(range.split('-')[0]);
          var max = parseInt(range.split('-')[1]);
          if (value.length >= min && value.length <= max) return true; else return false ;
      }
      function isInt(value) {
          if (rule.int.test(value)) return true; else return false ;
      }
      function isUname(value) {
          if (rule.uname.test(value)) return true; else return false ;
      }
      function isZh(value) {
          if (rule.zh.test(value)) return true; else return false ;
      }
  </script>

</myfooter>
</body>
</html>