<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>选择在线支付方式 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/order-confirm.css">
</head>
<body>
<!-- 轮播top菜单导航引入 -->
<jsp:include page="/WEB-INF/views/modules/common/site_header.jsp" />
<!-- 轮播top菜单导航引入 -->

<div class="page-main page-mini-main">
  <div class="container-fluid confirm-box">
    <form target="_blank" action="#" id="J_payForm" method="post">
      <div class="section section-order">
        <div class="order-info clearfix">
          <div class="fl">
            <h2 class="title">订单提交成功！去付款咯～</h2>
            <p class="order-time" id="J_deliverDesc">我们将尽快为您发货</p>
            <p class="order-time">请在下单后<span class="pay-time-tip">24小时</span>内完成支付, 超时后将取消订单</p>
            <p class="post-info" id="J_postInfo"> 收货信息：${orderShipment.userName} ${orderShipment.userPhone}&nbsp;&nbsp;
              ${orderShipment.provinceName}&nbsp;&nbsp;${orderShipment.cityName}&nbsp;&nbsp;${orderShipment.districtName}&nbsp;&nbsp;${orderShipment.userAdress} </p>
          </div>
          <div class="fr">
            <p class="total"> 应付总额：<span class="money"><em>${order.payAmount }</em>元</span> </p>
            <a href="javascript:void(0);" class="show-detail" id="J_showDetail">订单详情&nbsp;<i class="glyphicon glyphicon-chevron-down"></i></a> </div>
        </div>
        <i class="iconfont icon-right">&#x221a;</i>
        <div class="order-detail">
          <ul>
            <li class="clearfix">
              <div class="label">订单号：</div>
              <div class="content"> <span class="order-num" data-order-number="${order.orderNumber}"> ${order.orderNumber } </span> </div>
            </li>
            <li class="clearfix">
              <div class="label">收货信息：</div>
              <div class="content"> ${orderShipment.userName} ${orderShipment.userPhone}&nbsp;&nbsp;
                ${orderShipment.provinceName}&nbsp;&nbsp;${orderShipment.cityName}&nbsp;&nbsp;${orderShipment.districtName}&nbsp;&nbsp;${orderShipment.userAdress} </div>
            </li>
            <li class="clearfix">
              <div class="label">商品名称：</div>
              <div class="content">
                <c:forEach items="${orderProducts}" var="orderProduct"> ${orderProduct.name}&nbsp; <br>  <!--$-{orderProduct.productSpecName}-->
                </c:forEach>
              </div>
            </li>
            <li class="clearfix">
              <div class="label">配送时间：</div>
              <div class="content">
                <c:if test="${order.shipmentTime eq 1}">不限送货时间</c:if>
                <c:if test="${order.shipmentTime eq 2}">工作日送货</c:if>
                <c:if test="${order.shipmentTime eq 3}">双休日、假日送货</c:if>
              </div>
            </li>
            <li class="clearfix">
              <div class="label">发票信息：</div>
              <div class="content">
                <c:if test="${order.invoiceType eq 1}">不开发票</c:if>
                <c:if test="${order.invoiceType eq 2}">电子发票&nbsp; ${order.invoiceTitle }</c:if>
                <c:if test="${order.invoiceType eq 3}">普通发票&nbsp; ${order.invoiceTitle }</c:if>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="section section-payment">
        <div class="cash-title" id="J_cashTitle"> 选择以下支付方式付款 </div>
        <div class="payment-box ">
          <div class="payment-header clearfix">
            <h3 class="title">支付平台</h3>
            <span class="desc"></span> </div>
          <div class="payment-body">
            <ul class="clearfix payment-list J_paymentList J_linksign-customize" >
              <li class="J_bank">
                <input type="radio" name="payOnlineBank" id="weixinpay" value="weixinpay" />
               <a href="javascript:payOrder('weixinpay');" > <img src="${ctx}/static/os/images/wechat.jpg" alt="微信支付" style="margin-left: 0;"/></a></li>
              <li class="J_bank">
                <input type="radio" name="payOnlineBank" id="alipay" value="alipay" />
                <a href="javascript:payOrder('alipay');"><img src="${ctx}/static/os/images/alipay.png" alt="支付宝" style="margin-left: 0;"/></a></li>
            </ul>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="J_payOrderDiv" class="modal modal-verify modal-hide in disabled" aria-hidden="false" style=" border:1px solid #ff6700; display: none;" >
  <div class="modal-hd">
    <h3 class="title">微信支付二维码</h3>
    <a class="close" data-dismiss="modal" href="javascript: void(0);"><i class="glyphicon glyphicon-remove"></i></a> </div>
  <div class="modal-bd">
      <div  style="text-align: center;height: 250px;">
        <%--<img src='/pay/wxpay_qrcode.htm?orderNumber=" + oid + "&rand=" + new Date() / 100 + "'/>--%>
        <img id="pay_qr_img" src=''/>
      </div>
  </div>
  <div class="modal-ft"> <a id="J_paySuccess" class="btn btn-gray" data-dismiss="modal" href="javascript: void(0);">支付成功</a> <a id="J_payFail" class="btn btn-primary" href="javascript: void(0);">支付失败</a> </div>
</div>

<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script> 
  <script src="${ctxsta}/os/js/order.js"></script>
  <script>
      $(function() {
          $("#J_paySuccess").on("click", function() {
              $("#J_payOrderDiv").hide();
               window.location.href = baselocation + '/uc/order/list';
          })

          $("#J_payFail").on("click", function() {
              $("#J_payOrderDiv").hide();

          })

          $(".close").on("click", function() {
              $("#J_payOrderDiv").hide();
          })
      })

  </script>
</myfooter>
</body>
</html>