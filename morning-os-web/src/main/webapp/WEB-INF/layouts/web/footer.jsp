<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>

<!--     底部信息  begin       -->
<div class="site-footer">
  <div class="container-fluid">
    <div class="footer-service">
      <ul class="list-service clearfix">
        <li> <a rel="nofollow" ><span class="glyphicon glyphicon-wrench"></span>正品质量保证</a> </li>
        <li> <a rel="nofollow" ><span class="glyphicon glyphicon-phone-alt"></span>7天无理由退货</a> </li>
        <li> <a rel="nofollow" ><span class="glyphicon glyphicon-usd"></span>15天免费换货</a> </li>
        <li> <a rel="nofollow" ><span class="glyphicon glyphicon-gift"></span>便捷在线支付</a> </li>
        <li> <a rel="nofollow" ><span class="glyphicon glyphicon-map-marker"></span>官方自营网站</a> </li>
      </ul>
    </div>
    <%--<div class="footer-links clearfix">--%>
      <%--<dl class="col-links col-links-first">--%>
        <%--<dt>帮助中心</dt>--%>
        <%--<dd><a rel="nofollow" href="#">购物指南</a></dd>--%>
        <%--<dd><a rel="nofollow" href="#">支付方式</a></dd>--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">配送方式</a></dd>&ndash;%&gt;--%>
      <%--</dl>--%>
      <%--<dl class="col-links ">--%>
        <%--<dt>服务支持</dt>--%>
        <%--<dd><a rel="nofollow" href="#">售后政策</a></dd>--%>
        <%--<dd><a rel="nofollow" href="#">自助服务</a></dd>--%>
      <%--</dl>--%>
      <%--&lt;%&ndash;<dl class="col-links ">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dt>购物之家</dt>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">服务网点</a></dd>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">预约服务</a></dd>&ndash;%&gt;--%>
      <%--&lt;%&ndash;</dl>&ndash;%&gt;--%>
      <%--<dl class="col-links ">--%>
        <%--<dt>关于我们</dt>--%>
        <%--<dd><a rel="nofollow" href="#">了解我们</a></dd>--%>
        <%--<dd><a rel="nofollow" href="#">联系我们</a></dd>--%>
      <%--</dl>--%>
      <%--&lt;%&ndash;<dl class="col-links ">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dt>关注我们</dt>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">新浪微博</a></dd>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">官方微信</a></dd>&ndash;%&gt;--%>
      <%--&lt;%&ndash;</dl>&ndash;%&gt;--%>
      <%--&lt;%&ndash;<dl class="col-links ">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dt>其它服务</dt>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">F 码通道</a></dd>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<dd><a rel="nofollow" href="#">防伪查询</a></dd>&ndash;%&gt;--%>
      <%--&lt;%&ndash;</dl>&ndash;%&gt;--%>
      <%--<div class="col-contact">--%>
        <%--<p class="phone"> 166-6666-6666 </p>--%>
        <%--<p> <span class="J_serviceTime-normal" style="">周一至周日 8:00-18:00</span> <span class="J_serviceTime-holiday" style="display: none;">2月7日至13日服务时间 9:00-18:00</span><br>--%>
          <%--（仅收市话费） </p>--%>
        <%--<a rel="nofollow" class="btn btn-line-primary btn-small" href="#"><span class="glyphicon glyphicon-earphone"></span> 24小时在线客服</a> </div>--%>
    <%--</div>--%>
  </div>
</div>
<!--     底部信息  end         --> 

<!--     公司链接  begin       -->
<div class="site-info">
  <div class="container-fluid">
    <%--<div class="logo"> <img src="${ctxsta}/os/images/logo.png"> </div>--%>
    <div class="info-text">
      <p class="sites">
        <c:forEach items="${indexBottom}" var="indexBottom"> <a href="${indexBottom.href}" target="${indexBottom.target}">${indexBottom.name}</a><span class="sep">|</span> </c:forEach>
      </p>
      <%--<p> © 2016<a href=""> 购物网 </a> <!--/ <span id="showsectime"></span> / <span id="TimeShow"></span>-->--%>
      <%--<p>--%>
      <p> © <a href="#">shop.com</a> 浙ICP证123456号 浙ICP备12345678号 浙公网安备123456789XXX号 </p>
    </div>
    <div class="info-links"> <a href="#"><img src="${ctxsta}/os/images/v-logo-2.png" alt="诚信网站"></a> <a href="#"><img src="${ctxsta}/os/images/v-logo-1.png" alt="可信网站"></a> <a href="#"><img src="${ctxsta}/os/images/v-logo-3.png" alt="网上交易保障中心"></a> </div>
  </div>
  <%--<div class="word">精品好物在这里</div>--%>
</div>
<!--     公司链接  end         --> 

<!--     返回顶部  begin       -->
<%--<div class="social-share" data-sites="qq, wechat, qzone"></div>--%>
<div class="back-to-top">
  <div class="container-fluid">
    <p id="back-to-top"><a href="#top"><span class="glyphicon glyphicon-menu-up" style="font-size:30px;color:#ff6700;"></span>回到顶部</a></p>
  </div>
</div>
<!--     返回顶部  end         --> 