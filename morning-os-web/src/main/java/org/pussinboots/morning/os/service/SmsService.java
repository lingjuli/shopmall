package org.pussinboots.morning.os.service;

/**
 * 发送短信服务
 *
 */
public interface SmsService {

/**
 * 发送短信
 * @param mobile  手机号
 * @param content  短信内容
 */
	 void sendSMS(String mobile, String content);
}
