package org.pussinboots.morning.os.common.sms;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PuXunSmsUtils {
	private static String url = "http://116.62.244.37/yqx/v1/sms/single_send";
	private static String account = "7308";
	private static String apikey = "71525a25918c6048fce771a6cd8e3559";

	/**
     * 普讯发送短信
	 * @param mobile  手机号
	 * @param content  短信内容
	 */
	public static void sendSMS(String mobile,String content){
		/** 测试 */
		try {
			String sign = Base64.encodeBase64String(DigestUtils.md5Hex(
					account + apikey).getBytes());

			JSONObject json = new JSONObject();
			json.put("account", account);
			json.put("mobile", mobile);
			json.put("text", content);
			json.put("sign", sign);
			json.put("extend", "");

			System.out.println("request:" + json.toJSONString());

			// 设置PostMethod
			PostMethod postMethod = new PostMethod(url);
			postMethod.getParams().setContentCharset("utf-8");
			postMethod.getParams().setHttpElementCharset("utf-8");

			// 设置requestEntity
			RequestEntity requestEntity = new StringRequestEntity(
					json.toJSONString(), "application/json", "UTF-8");
			postMethod.setRequestEntity(requestEntity);

			// http client 参数设置 连接超时 读取数据超时
			HttpClient httpClient = new HttpClient();
			// httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
			// httpClient.getHttpConnectionManager().getParams().setSoTimeout(timeout);
			try {
				httpClient.executeMethod(postMethod);
				if (postMethod.getStatusCode() == HttpStatus.SC_OK) {
					InputStream resStream = postMethod
							.getResponseBodyAsStream();
					BufferedReader br = new BufferedReader(
							new InputStreamReader(resStream, "UTF-8"));
					StringBuffer resBuffer = new StringBuffer();
					String resTemp = "";
					while ((resTemp = br.readLine()) != null) {
						resBuffer.append(resTemp);
					}
					String result = resBuffer.toString();
					System.out.println(result);
				}
			} finally {
				postMethod.releaseConnection();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException {
		/** 测试 */
		try {
			String sign = Base64.encodeBase64String(DigestUtils.md5Hex(
					account + apikey).getBytes());

			JSONObject json = new JSONObject();
			json.put("account", account);
			json.put("mobile", "18690722566");
			json.put("text", "puxun testing");
			json.put("sign", sign);
			json.put("extend", "");

			System.out.println("request:" + json.toJSONString());

			// 设置PostMethod
			PostMethod postMethod = new PostMethod(url);
			postMethod.getParams().setContentCharset("utf-8");
			postMethod.getParams().setHttpElementCharset("utf-8");

			// 设置requestEntity
			RequestEntity requestEntity = new StringRequestEntity(
					json.toJSONString(), "application/json", "UTF-8");
			postMethod.setRequestEntity(requestEntity);

			// http client 参数设置 连接超时 读取数据超时
			HttpClient httpClient = new HttpClient();
			// httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
			// httpClient.getHttpConnectionManager().getParams().setSoTimeout(timeout);
			try {
				httpClient.executeMethod(postMethod);
				if (postMethod.getStatusCode() == HttpStatus.SC_OK) {
					InputStream resStream = postMethod
							.getResponseBodyAsStream();
					BufferedReader br = new BufferedReader(
							new InputStreamReader(resStream, "UTF-8"));
					StringBuffer resBuffer = new StringBuffer();
					String resTemp = "";
					while ((resTemp = br.readLine()) != null) {
						resBuffer.append(resTemp);
					}
					String result = resBuffer.toString();
					System.out.println(result);
				}
			} finally {
				postMethod.releaseConnection();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}