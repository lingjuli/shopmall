package org.pussinboots.morning.os.controller.order;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.common.constant.CommonReturnCode;
import org.pussinboots.morning.common.enums.StatusEnum;
import org.pussinboots.morning.common.pay.alipay.PayCallbackParam;
import org.pussinboots.morning.common.pay.alipay.AlipayConfig;
import org.pussinboots.morning.common.pay.alipay.AlipayNotify;
import org.pussinboots.morning.common.pay.wxpay.QRCodeUtil;
import org.pussinboots.morning.common.util.MoneyUtil;
import org.pussinboots.morning.order.common.enums.OrderStatusEnum;
import org.pussinboots.morning.order.entity.Order;
import org.pussinboots.morning.order.entity.OrderProduct;
import org.pussinboots.morning.order.entity.OrderShipment;
import org.pussinboots.morning.order.entity.PayDO;
import org.pussinboots.morning.order.pojo.vo.OrderShoppingCartVO;
import org.pussinboots.morning.order.pojo.vo.OrderVO;
import org.pussinboots.morning.order.service.IOrderProductService;
import org.pussinboots.morning.order.service.IOrderService;
import org.pussinboots.morning.order.service.IOrderShipmentService;
import org.pussinboots.morning.order.service.PayService;
import org.pussinboots.morning.os.common.result.OsResult;
import org.pussinboots.morning.os.common.util.SingletonLoginUtils;
import org.pussinboots.morning.product.entity.Product;
import org.pussinboots.morning.product.entity.ProductSpecification;
import org.pussinboots.morning.product.pojo.vo.CartVO;
import org.pussinboots.morning.product.pojo.vo.ShoppingCartVO;
import org.pussinboots.morning.product.service.IProductService;
import org.pussinboots.morning.product.service.IProductSpecificationService;
import org.pussinboots.morning.product.service.IShoppingCartService;
import org.pussinboots.morning.user.entity.Address;
import org.pussinboots.morning.user.service.IAddressService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.imageio.ImageIO;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：OrderBuyController   
* 类描述：商品购买表示层控制器         
* 创建人：代远航
* 创建时间：2019年5月10日 下午11:08:49
*
 */
@Controller
@RequestMapping(value = "/buy")
@Api(value = "商品购买", description = "商品购买")
public class OrderBuyController extends BaseController {

	private static final String SUCCESS_RESP = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";

	@Autowired
	private IAddressService addressService;
	@Autowired
	private IShoppingCartService shoppingCartService;
	@Autowired
	private IOrderService orderService;
	@Autowired
	private IOrderProductService orderProductService;
	@Autowired
	private IOrderShipmentService orderShipmentService;
	@Autowired
	private IProductSpecificationService productSpecificationService;
	@Autowired
	private IProductService productService;
	@Autowired
	private PayService payService;


	/**
	 * GET 填写订单信息
	 * @return
	 */
	@ApiOperation(value = "填写订单信息", notes = "填写订单信息")
	@GetMapping(value = "/judgeProductStock")
	@ResponseBody
	public Object judgeProductStock() {

		// 购物车选中商品
		CartVO cartVO = shoppingCartService.list(SingletonLoginUtils.getUserId(), StatusEnum.CHECKED.getStatus());
		List<ShoppingCartVO> shoppingCartVOs=cartVO.getShoppingCartVOs();
		boolean stackflag=true;
		String stackmsg="";
		for(ShoppingCartVO item: shoppingCartVOs){
			ProductSpecification ps=productSpecificationService.getProductSpecification(item.getProductSpecNumber());
			if(ps.getStock()<=0 ||  ps.getStock()<item.getBuyNumber()){
				 stackflag=false;
				 Product p=productService.selectById(ps.getProductId());
				 int stacknum=0;
				 if(ps.getStock()>=0){
					 stacknum=ps.getStock();
				 }
				 stackmsg="库存不足："+p.getName()+ps.getSpecName()+" 的当前库存数量为"+stacknum+"，请更改要购买的商品与数量";
				 break;
			}
		}
		if(stackflag){
			return new OsResult(CommonReturnCode.SUCCESS);
		}else{
			return new OsResult(CommonReturnCode.BAD_REQUEST.getCode(), stackmsg);

		}
	}
	
	/**
	 * GET 填写订单信息
	 * @return
	 */
	@ApiOperation(value = "填写订单信息", notes = "填写订单信息")
	@GetMapping(value = "/checkout")
	public String checkout(Model model) {
		// 收获地址
		List<Address> addresses = addressService.listAddress(SingletonLoginUtils.getUserId());
		model.addAttribute("addresses", addresses);

		// 购物车选中商品
		CartVO cartVO = shoppingCartService.list(SingletonLoginUtils.getUserId(), StatusEnum.CHECKED.getStatus());
		model.addAttribute("cartVO", cartVO);

		return "/modules/order/order_buy_checkout";
	}
	
	/**
	 * POST 提交订单
	 * @return
	 */
	@ApiOperation(value = "提交订单", notes = "提交订单")
	@PostMapping(value = "/confirm")
	@ResponseBody
	public Object confirm(Order order, @RequestParam(value = "addressId", required = true) Long addressId) {
		// 收货地址
		Address address = addressService.getAddress(addressId, SingletonLoginUtils.getUserId());
		if (address != null) {
			OrderShipment orderShipment = new OrderShipment();
			BeanUtils.copyProperties(address, orderShipment);

			// 购物车选中商品
			CartVO cartVO = shoppingCartService.list(SingletonLoginUtils.getUserId(), StatusEnum.CHECKED.getStatus());
			if (!cartVO.getShoppingCartVOs().isEmpty()) {
				order.setBuyNumber(cartVO.getTotalNumber());// 订单总数量
				order.setOrderAmount(cartVO.getTotalPrice());// 订单总价格
				order.setOrderScore(cartVO.getTotalScore());// 订单总积分

				// 遍历购物车选中商品列表
				List<OrderShoppingCartVO> orderShoppingCartVOs = new ArrayList<OrderShoppingCartVO>();
				for (ShoppingCartVO vo : cartVO.getShoppingCartVOs()) {
					OrderShoppingCartVO orderShoppingCartVO = new OrderShoppingCartVO();
					BeanUtils.copyProperties(vo, orderShoppingCartVO);
					orderShoppingCartVOs.add(orderShoppingCartVO);
				}
				Long orderNumber = orderService.insertOrder(order, orderShipment, orderShoppingCartVOs,
						SingletonLoginUtils.getUserId());

				if (orderNumber != null) {
					shoppingCartService.deleteCheckStatus(SingletonLoginUtils.getUserId());
					return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
					// TODO Long
					// 17位传送末尾精读损失,例14944366378872495前台接收变成14944366378872494,解决方法toString,原因未知
				} else {
					return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),
							CommonReturnCode.UNKNOWN_ERROR.getMessage());
				}
			} else {
				return new OsResult(CommonReturnCode.BAD_REQUEST.getCode(), "请选择想要购买的商品!");
			}
		} else {
			return new OsResult(CommonReturnCode.BAD_REQUEST.getCode(), "请选择正确的收货地址!");
		}
	}
	
	/**
	 * GET 确认订单
	 * @return
	 */
	@ApiOperation(value = "确认订单", notes = "确认订单")
	@GetMapping(value = "/confirm/{orderNumber}")
	public String confirmShow(Model model, @PathVariable("orderNumber") Long orderNumber) {

		Order order = orderService.getByOrderNumber(orderNumber, SingletonLoginUtils.getUserId(),
				OrderStatusEnum.SUBMIT_ORDERS.getStatus());

		if (order != null) {

			List<OrderProduct> orderProducts = orderProductService.listByOrderId(order.getOrderId());

			OrderShipment orderShipment = orderShipmentService.getByOrderId(order.getOrderId());

			model.addAttribute("order", order); // 订单信息
			model.addAttribute("orderProducts", orderProducts);// 订单详情表
			model.addAttribute("orderShipment", orderShipment);// 订单配送表
		}

		return "/modules/order/order_buy_confirm";
	}



	private void getQRCode(HttpServletResponse response, String msg) {
		response.setContentType("image/png");
		// BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
		BufferedImage test = QRCodeUtil.genQRCode(msg);
        System.out.println("图片高度："+test.getHeight()+"===========");
		System.out.println("图片宽度："+test.getWidth()+"===========");
		System.out.println("图片字符串："+test.toString()+"===========");
		try {
			ImageIO.write(test, "PNG", response.getOutputStream());// 将内存中的图片通过流动形式输出到客户端
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@ApiOperation(value = "选择微信支付订单", notes = "选择微信支付订单")
	@RequestMapping(value = "/wxpay_qrcode.htm")
	public String wxpayOrder(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "orderNumber", required = true) Long orderNumber, Model model) {

		OrderVO order=orderService.getOrder(SingletonLoginUtils.getUserId(),orderNumber);
		if (order == null) {
			return null;
		}
		if(order.getOrderStatus()!=OrderStatusEnum.SUBMIT_ORDERS.getStatus()){  //不等于 已提交订单状态（待支付）
			return null;
		}
		Long userId=SingletonLoginUtils.getUserId();
		String  body="订单支付:"+orderNumber;

		String payTradeNo =payService.savePayRecord(order,userId,2); //微信支付

		String payAmountStr=order.getPayAmount().toString();
		int totalFee= MoneyUtil.yuanToFen(payAmountStr);
		//getQRCode(response, WxPayUtil.wxPrepay(payTradeNo, totalFee, body));
		getQRCode(response, body+"-----金额："+payAmountStr+"元----"+ new Date().toString());
		return null;
	}

	/**
	 * 微信回调处理函数
	 */
	@RequestMapping(value = "/wxpay_callback.htm", method = RequestMethod.POST)
	public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ServletInputStream inputStream = null;
		try {
			inputStream = request.getInputStream();
		} catch (IOException e1) {
			logger.error("", e1);
		}

		int contentLength = request.getContentLength();
		byte[] byteData = new byte[contentLength];
		try {
			inputStream.read(byteData, 0, contentLength);
		} catch (IOException e) {
			logger.error("", e);
		}

		String xmlString = null;
		try {
			xmlString = new String(byteData, "utf-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
		}

		logger.info("wxpay_callback, {}", xmlString);

		PayDO pay= payService.wxCallBack(xmlString);
		if(pay!=null && pay.getStatus()==1){  //支付成功
			Long  orderNumber=pay.getOrderNumber();
			payService.clearPayRecord(orderNumber);  //清理多余的无效支付信息（）
			//支付成功后处理放在异步返回
			Integer count=orderService.payOrder(orderNumber,SingletonLoginUtils.getUserId());
			if(count>0){
				//库存和销量---库存和销量由每个规格确定
				Order order=orderService.getByOrderNumber(orderNumber);
				List<OrderProduct> oplist=orderProductService.listByOrderId(order.getOrderId());
				for(OrderProduct op: oplist){  //循环更改每一个产品规格的销量和库存
					ProductSpecification ps=productSpecificationService.getProductSpecification(op.getProductSpecNumber());
					ps.setSalesVolume(ps.getSalesVolume()+op.getBuyNumber());
					ps.setStock(ps.getStock()-op.getBuyNumber());
					productSpecificationService.updateById(ps);
				}
			}
		}

		responseString(response, SUCCESS_RESP);
	}

	public void responseString(HttpServletResponse response, String content) throws IOException {
		response.setContentType("application/xml;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(content);
		response.getWriter().close();
	}


	/**
	 * POST 支付订单
	 * @return
	 */
	@ApiOperation(value = "选择支付宝支付订单", notes = "选择支付宝支付订单")
	@RequestMapping(value = "/payOrder")
	public String payOrder(@RequestParam(value = "orderNumber", required = true) Long orderNumber, Model model) {

		OrderVO order=orderService.getOrder(SingletonLoginUtils.getUserId(),orderNumber);
		if (order == null) {
			return null;
		}
		if(order.getOrderStatus()!=OrderStatusEnum.SUBMIT_ORDERS.getStatus()){  //已提交订单状态（待支付）
			return null;
		}
		Long userId=SingletonLoginUtils.getUserId();

		//把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<>();
		sParaTemp.put("service", AlipayConfig.service);
		sParaTemp.put("partner", AlipayConfig.partner);
		sParaTemp.put("seller_id", AlipayConfig.seller_id);
		sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", AlipayConfig.payment_type);
		sParaTemp.put("return_url", AlipayConfig.return_url);
		sParaTemp.put("notify_url", AlipayConfig.notify_url);
		sParaTemp.put("anti_phishing_key", AlipayConfig.anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", AlipayConfig.exter_invoke_ip);
		sParaTemp.put("out_trade_no",orderNumber.toString());
		sParaTemp.put("subject",  "订单支付:"+orderNumber);
		sParaTemp.put("total_fee", order.getPayAmount().toString());  //元
		sParaTemp.put("body", "");
		String payTradeNo =payService.savePayRecord(order,userId,1); //支付宝支付
		sParaTemp.put("out_trade_no", payTradeNo);

		model.addAttribute("alipayForm", payService.buildAlipayRequestForm(sParaTemp, "post", "确认支付"));
		return "/modules/order/pay";
	}


	@ApiOperation(value = "支付宝支付同步返回", notes = "支付宝支付同步返回")
	@RequestMapping(value = "/alipay_return.htm")
	public String alipayReturn() throws Exception {
		return "/modules/order/pay_return";
	}

	@ApiOperation(value = "支付宝支付异步返回", notes = "支付宝支付异步返回")
	@RequestMapping(value = "/alipay_callback.htm", method = RequestMethod.POST)
	public void alipayCallback(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, String> params = new HashMap<>();
		Map requestParams = request.getParameterMap();
		for (Object o : requestParams.keySet()) {
			String name = (String) o;
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}
		logger.info("receive alipay callback:{}", params);

		//验证签名
		boolean verify = AlipayNotify.verify(params);
		if (verify) {
			//商户订单号，商户网站订单系统中唯一订单号，必填
			String outTradeNo = request.getParameter("out_trade_no");
			//支付宝交易号
			String tradeNo = request.getParameter("trade_no");
			//交易状态
			String tradeStatus = request.getParameter("trade_status");
			//金额
			String totalFee = request.getParameter("total_fee");
			//用户支付账号
			String buyerEmail = request.getParameter("buyer_email");

//        outTradeNo = "4325435_etre_231112";
//        tradeNo = "2016040521001004260258979160";
//        tradeStatus = "TRADE_SUCCESS";
//        totalFee = "0.01";
//        buyerEmail = "1111@163.com";
			PayCallbackParam param = new PayCallbackParam();
			param.setOutTradeNo(tradeNo);
			param.setBuyerEmail(buyerEmail);
			param.setChannelId(1);
			param.setTradeStatus(tradeStatus);
			param.setPayTradeNo(outTradeNo);
			param.setTotalFee(totalFee);
			Long orderNumber=payService.payCallback(param);

			if ("TRADE_SUCCESS".equals(tradeStatus)) {
				payService.clearPayRecord(orderNumber);  //清理多余的无效支付信息（）
				//支付成功后处理放在异步返回
				Integer count=orderService.payOrder(orderNumber,SingletonLoginUtils.getUserId());
				if(count>0){
					//库存和销量---库存和销量由每个规格确定
					Order order=orderService.getByOrderNumber(orderNumber);
					List<OrderProduct> oplist=orderProductService.listByOrderId(order.getOrderId());
					for(OrderProduct op: oplist){  //循环更改每一个产品规格的销量和库存
						ProductSpecification ps=productSpecificationService.getProductSpecification(op.getProductSpecNumber());
						ps.setSalesVolume(ps.getSalesVolume()+op.getBuyNumber());
						ps.setStock(ps.getStock()-op.getBuyNumber());
						productSpecificationService.updateById(ps);
					}
				}
			}
		} else {
			logger.warn("receive alipay callback, get wrong sign:{}", params);
		}

		//通知支付宝成功
		response.getWriter().write("success");
		response.getWriter().close();

	}


	/**
	 * POST 支付订单-----模拟一次性支付成功
	 * @return
	 */
	@ApiOperation(value = "支付订单", notes = "支付订单")
	@PostMapping(value = "/payOrderBak")
	@ResponseBody
	public Object payOrderBak(@RequestParam(value = "orderNumber", required = true) Long orderNumber,
						   @RequestParam(value = "payType", required = true) String payType) {
		Integer count=orderService.payOrder(orderNumber,SingletonLoginUtils.getUserId());

		if(count>0){
			//库存和销量---库存和销量由每个规格确定
			OrderVO order=orderService.getOrder(SingletonLoginUtils.getUserId(),orderNumber);
			List<OrderProduct> oplist=orderProductService.listByOrderId(order.getOrderId());
			for(OrderProduct op: oplist){  //循环更改每一个产品规格的销量和库存
				ProductSpecification ps=productSpecificationService.getProductSpecification(op.getProductSpecNumber());
				ps.setSalesVolume(ps.getSalesVolume()+op.getBuyNumber());
				ps.setStock(ps.getStock()-op.getBuyNumber());
				productSpecificationService.updateById(ps);
			}
			//退款或退货后恢复对应商品的销量和库存
			return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
		}else{
			return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),CommonReturnCode.UNKNOWN_ERROR.getMessage());
		}
	}


}
