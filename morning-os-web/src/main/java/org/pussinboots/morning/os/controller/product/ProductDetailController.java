package org.pussinboots.morning.os.controller.product;

import java.util.List;

import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.common.enums.StatusEnum;
import org.pussinboots.morning.os.common.upload.UploadManager;
import org.pussinboots.morning.os.service.SmsService;
import org.pussinboots.morning.product.common.constant.ProductConstantEnum;
import org.pussinboots.morning.product.entity.Category;
import org.pussinboots.morning.product.entity.ProductAttribute;
import org.pussinboots.morning.product.entity.ProductImage;
import org.pussinboots.morning.product.entity.ProductParameter;
import org.pussinboots.morning.product.pojo.dto.ProductSpecificationDTO;
import org.pussinboots.morning.product.pojo.vo.ProductVO;
import org.pussinboots.morning.product.service.ICategoryService;
import org.pussinboots.morning.product.service.IProductAttributeService;
import org.pussinboots.morning.product.service.IProductImageService;
import org.pussinboots.morning.product.service.IProductParameterService;
import org.pussinboots.morning.product.service.IProductService;
import org.pussinboots.morning.product.service.IProductSpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;

/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：ProductDetailControlller   
* 类描述：商品详情表示层控制器   
* 创建人：代远航
* 创建时间：2019年4月24日 下午11:24:37
*
 */
@Controller
@RequestMapping(value = "/detail")
@Api(value = "商品详情", description = "商品详情")
public class ProductDetailController extends BaseController {
	
	@Autowired
	private IProductService productService;
	@Autowired
	private ICategoryService categoryService;
	@Autowired
	private IProductAttributeService productAttributeService;
	@Autowired
	private IProductImageService productImageService;
	@Autowired
	private IProductParameterService productParameterService;
	@Autowired
	private IProductSpecificationService productSpecificationService;
	@Autowired
	private SmsService smsService;
	
	/**
	 * GET 商品详情页面
	 * @return
	 */
	@ApiOperation(value = "商品详情页面", notes = "根据传过来的商品编号获取商品详情信息")  
	@GetMapping(value="/{productNumber}")
	public String item(Model model, @PathVariable("productNumber") Long productNumber, HttpServletRequest request) {
		// 根据商品编号查找商品信息
		ProductVO product = productService.getByNumber(productNumber, StatusEnum.SHELVE.getStatus());
		if(product != null) {
			 //网页显示的详情图片路径修正，加部署项目名称
			String oldimgpath="/uploads/images/goods/";
			String newimgpath="/shopmall/uploads/images/goods/";  //显示系统的网页图片路径修正
			String productdetail=product.getDescription();
			if(productdetail!=null && !"".equals(productdetail)){
				productdetail=productdetail.replaceAll(oldimgpath,newimgpath);
			}
			product.setDescription(productdetail);

			// 根据类目ID查找上级类目列表
			List<Category> upperCategories = categoryService.listUpperByProductId(product.getProductId(),
					StatusEnum.SHOW.getStatus());		
			
			// 根据商品ID查找商品属性
			ProductAttribute productAttribute = productAttributeService.getByProductId(product.getProductId());
			if(productAttribute!=null ){
				productAttribute.setPageViews(productAttribute.getPageViews()+1);
				productAttributeService.updateById(productAttribute);
			}

			// 根据商品ID查找商品展示图片
			List<ProductImage> productImages = productImageService.listByProductId(product.getProductId(),
					ProductConstantEnum.PRODUCT_PICIMG_NUMBER.getValue(), StatusEnum.SHOW.getStatus());
			model.addAttribute("productImages", productImages);
			for(ProductImage img: productImages){
				UploadManager.downloadpic(UploadManager.uploadPath,request.getSession().getServletContext().getRealPath("/"),img.getPicImg());
			}

			// 根据商品ID查找商品详情图片
			List<ProductImage> detailImages = productImageService.listByProductId(product.getProductId(),
					100, StatusEnum.HIDDEN.getStatus());
			for(ProductImage img: detailImages){  //无需upload目录
				UploadManager.downDetailpic(UploadManager.uploadPath,request.getSession().getServletContext().getRealPath("/"),img.getPicImg());
			}
			
			// 根据商品ID查找商品参数
			List<ProductParameter> productParameters = productParameterService.listByProductId(product.getProductId(),
					StatusEnum.SHOW.getStatus());
			System.out.println("==================获取了商品属性=================================");
			
			// 根据商品ID查找商品类型列表以及商品规格列表
			ProductSpecificationDTO productSpecificationDTO = productSpecificationService
					.getByProductId(product.getProductId(), StatusEnum.SHOW.getStatus());

			System.out.println("==================获取了商品规格=================================");
			model.addAttribute("product", product);// 商品信息
			model.addAttribute("upperCategories", upperCategories);// 上级类目列表
			model.addAttribute("productAttribute", productAttribute);// 商品属性
			model.addAttribute("productParameters", productParameters);// 商品参数
			model.addAttribute("kindVOs", productSpecificationDTO.getKindVOs());// 商品类型列表
			model.addAttribute("productSpecifications", JSON.toJSON(productSpecificationDTO.getProductSpecifications()));
		}

		//发送短信测试
		//System.out.println("---发送短信测试----");
		//smsService.sendSMS("18690722566","发送短信测试");
		
		return "/modules/product/product_detail";
	}

}
