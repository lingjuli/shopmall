package org.pussinboots.morning.os.controller.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.common.constant.CommonReturnCode;
import org.pussinboots.morning.common.enums.StatusEnum;
import org.pussinboots.morning.os.common.result.OsResult;
import org.pussinboots.morning.os.common.security.AuthorizingUser;
import org.pussinboots.morning.os.common.util.SingletonLoginUtils;
import org.pussinboots.morning.product.entity.FavoriteDO;
import org.pussinboots.morning.product.pojo.vo.CartVO;
import org.pussinboots.morning.product.pojo.vo.ProductVO;
import org.pussinboots.morning.product.pojo.vo.ShoppingCartVO;
import org.pussinboots.morning.product.service.FavoriteService;
import org.pussinboots.morning.product.service.IProductService;
import org.pussinboots.morning.product.service.IShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：ProductCartController   
* 类描述：商品购物车表示层控制器      
* 创建人：代远航
* 创建时间：2019年5月10日 下午3:47:16
*
 */
@Controller
@RequestMapping(value = "/favorite")
@Api(value = "商品收藏", description = "商品收藏")
public class ProductFavoriteController extends BaseController {
	
	@Autowired
	private IShoppingCartService shoppingCartService;

	@Autowired
	private FavoriteService favoriteService;
	@Autowired
	private IProductService productService;


	/**
	 * POST 判断是否是收藏商品
	 * @return
	 */
	@ApiOperation(value = "添加购物车商品", notes = "添加购物车商品")
	@GetMapping(value = "/isFavorite/{productNumber}")
	@ResponseBody
	public Object isFavorite(Model model,
			@PathVariable(value = "productNumber", required = true) Long productNumber) {

		AuthorizingUser authorizingUser = SingletonLoginUtils.getUser();
		if (authorizingUser != null) {
			Map<String, Object> params=new HashMap<>();
			params.put("productNumber",productNumber);
			params.put("userId",authorizingUser.getUserId());
			List<FavoriteDO> list=favoriteService.list(params);
			if(list!=null && list.size()>0){
				FavoriteDO favorite=list.get(0);
				if(favorite.getStatus()==1){
					return new OsResult(CommonReturnCode.SUCCESS,productNumber);
				}else{
					return new OsResult(CommonReturnCode.FAILED);
				}
			}else{
				return new OsResult(CommonReturnCode.FAILED);
			}
		} else {
			return new OsResult(CommonReturnCode.UNAUTHORIZED);
		}
	}


	/**
	 * POST 更改商品收藏状态
	 * @return
	 */
	@ApiOperation(value = "更改商品收藏状态", notes = "更改商品收藏状态")
	@PostMapping(value = "/changeFavorite/{productNumber}")
	@ResponseBody
	public Object changeFavorite(Model model,
							 @PathVariable(value = "productNumber", required = true) Long productNumber,@RequestParam(value = "operType", required = true)Boolean operType) {
		AuthorizingUser authorizingUser = SingletonLoginUtils.getUser();
		System.out.println("operType"+operType);
		if (authorizingUser != null) {
			Map<String, Object> params=new HashMap<>();
			params.put("productNumber",productNumber);
			params.put("userId",authorizingUser.getUserId());
			List<FavoriteDO> list=favoriteService.list(params);

			if(operType){ //需要移除
				System.out.println("需要移除");
				System.out.println("list--size:"+list.size());
				//Long[]  ids=new Long[list.size()];
				//for(int i=0;i<list.size();i++){
					//ids[i]=list.get(i).getFavoriteId();
					FavoriteDO  favorite=list.get(0);
					favorite.setStatus(2);
				    favorite.setCreateTime(new Date());
					favoriteService.update(favorite);
				//}
				return new OsResult(CommonReturnCode.SUCCESS);
			}else{  //需要增加
				if(list!=null && list.size()>0){  //存在记录,更改状态
						//ids[i]=list.get(i).getFavoriteId();
						FavoriteDO  favorite=list.get(0);
						favorite.setStatus(1);
					    favorite.setCreateTime(new Date());
						favoriteService.update(favorite);
				}else{
					ProductVO pv=productService.getByNumber(productNumber,1);
					FavoriteDO favorite=new FavoriteDO();
					favorite.setUserId(authorizingUser.getUserId());
					favorite.setProductId(pv.getProductId());
					favorite.setProductNumber(pv.getProductNumber());
					favorite.setName(pv.getName());
					favorite.setPicImg(pv.getPicImg());
					favorite.setShowPrice(pv.getShowPrice());
					favorite.setStatus(1);
					favorite.setCreateTime(new Date());
					favorite.setCreateBy(authorizingUser.getUserName());
					favorite.setBrandId(pv.getBrandId());
					favorite.setMobile(authorizingUser.getTelephone());
					favoriteService.save(favorite);
				}
				return new OsResult(CommonReturnCode.SUCCESS,productNumber);
			}
		} else {
			return new OsResult(CommonReturnCode.UNAUTHORIZED);
		}
	}
	

	

}
