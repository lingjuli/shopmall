package org.pussinboots.morning.os.common.util;

import java.util.regex.Pattern;

/**
 * @author bootdo
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils{

    /*方法二：推荐，速度最快
     * 判断是否为整数
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */
    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    public  static  String safeDealForUserInputMess(String  mess){
        mess=mess.replaceAll("drop","-drop-");
        mess=mess.replaceAll("creat","-creat-");
        mess=mess.replaceAll("update","-update-");
        mess=mess.replaceAll("delete","-delete-");
        mess=mess.replaceAll("select","-select-");
        mess=mess.replaceAll("alter","-alter-");
        return mess;
    }
}
