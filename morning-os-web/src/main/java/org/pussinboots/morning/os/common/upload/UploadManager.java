package org.pussinboots.morning.os.common.upload;

import com.alibaba.fastjson.JSONObject;

import org.pussinboots.morning.os.common.util.NewDateUtils;
import org.pussinboots.morning.os.common.util.NewImageUtils;
import org.pussinboots.morning.product.pojo.vo.CategoryVO;
import org.pussinboots.morning.product.pojo.vo.ProductVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 
* 项目名称：morning-common   
* 类名称：UploadManager   
* 类描述：文件上传支持类     
* 创建人：代远航
* 创建时间：2019年4月10日 上午3:24:42
*
 */
public class UploadManager {
	
	private static final Logger logger = LoggerFactory.getLogger(UploadManager.class);

	public static final String  uploadPath="c:/var/uploaded_files/";  //文件上传目录

	private UploadManager() { 
		throw new AssertionError();
	}

	public static void batdownloadpic(List<CategoryVO> categorys, HttpServletRequest request){
		//批量下载图片
		if(categorys!=null && categorys.size()>0){
			for(CategoryVO categoryVO:categorys){
				List<ProductVO> products=categoryVO.getProducts();
				if(products!=null && products.size()>0){
					for(ProductVO product: products){
						downloadpic(uploadPath,request.getSession().getServletContext().getRealPath("/"),product.getPicImg());
					}
				}
				List<CategoryVO>  childAdverts=categoryVO.getChildrenCategorys();
				if(childAdverts!=null && childAdverts.size()>0){
					batdownloadpic(childAdverts,request);
				}
			}
		}
	}



/**
* 复制单个文件
* @param oldPath String 原文件路径 如：c:/fqf.txt
* @param newPath String 复制后路径 如：f:/fqf.txt
* @return boolean
*/
	public static void copyFile(String oldPath, String newPath) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPath);
			if (oldfile.exists()) { //文件存在时
				InputStream inStream = new FileInputStream(oldPath); //读入原文件
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[1444];
				int length;
				while ( (byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread; //字节数 文件大小
					System.out.println(bytesum);
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
			}
		}
		catch (Exception e) {
			System.out.println("复制单个文件操作出错");
			e.printStackTrace();

		}

	}



	public static void copyFileUsingFileChannels(String oldPath, String newPath) throws IOException {
		File source = new File(oldPath);
		//
		String newDir = newPath.substring(0,newPath.lastIndexOf("/"));
		if(source.exists()  ){
			File newDirFile = new File(newDir);
			if(!newDirFile.exists()){
				newDirFile.mkdirs();
			}
			File dest = new File(newPath);
			FileChannel inputChannel = null;
			FileChannel outputChannel = null;
			try {
				inputChannel = new FileInputStream(source).getChannel();
				outputChannel = new FileOutputStream(dest).getChannel();
				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
			} finally {
				inputChannel.close();
				outputChannel.close();
			}
		}
	}



	/**
	 * 上传文件存放服务器
	 * @param request 服务器请求
	 * @param dir
	 * @return
	 */
	public static String getServerSaveDir(HttpServletRequest request, String dir, String belong) {
//		StringBuilder uploadPath = new StringBuilder(request.getSession().getServletContext().getRealPath("uploads"));
//		uploadPath.append(File.separator);
//		uploadPath.append(dir);
		StringBuilder uploadPath = new StringBuilder(request.getSession().getServletContext().getRealPath("images"));
		uploadPath.append(File.separator);
		uploadPath.append(belong);		
		uploadPath.append(File.separator);
		uploadPath.append(NewDateUtils.format(new Date(), "yyyyMMdd"));
		File file = new File(uploadPath.toString());
		if (!file.exists()) {
			file.mkdirs();
		}
		return file.getPath();
	}


	/**
	 * 上传文件存放服务器
	 * @param cpanpath 服务器请求
	 * @param dir
	 * @return
	 */
	public static String getServerSaveDirCpan(String cpanpath, String dir, String belong) {

		StringBuilder uploadPath = new StringBuilder(cpanpath);
		uploadPath.append(File.separator);
		uploadPath.append(dir);
		uploadPath.append(File.separator);
		uploadPath.append(belong);
		uploadPath.append(File.separator);
		uploadPath.append(NewDateUtils.format(new Date(), "yyyyMMdd"));
		File file = new File(uploadPath.toString());
		if (!file.exists()) {
			file.mkdirs();
		}
		return file.getPath();
	}
	
	/**
	 * 文件重命名
	 * @param file
	 * @return
	 */
	public static String rename(MultipartFile file) {
		// 获取原始文件名
		String fileName = file.getOriginalFilename();
		// 新文件名称，不设置时默认为原文件名
		return new Date().getTime() + (new Random().nextInt(9999 - 1000 + 1) + 1000)
				+ fileName.substring(fileName.lastIndexOf('.'));
	}
	
	/**
	 * 文件保存路径
	 * @param dir 一级目录
	 * @param belong 二级目录
	 * @param newFileName 文件名
	 * @return
	 */
	public static String getSavaDir(String dir, String belong, String newFileName) {
		StringBuilder savePath = new StringBuilder();
		savePath.append(dir);
		savePath.append(File.separator);
		savePath.append(belong);
		savePath.append(File.separator);
		savePath.append(NewDateUtils.format(new Date(), "yyyyMMdd"));
		savePath.append(File.separator);
		savePath.append(newFileName);// 文件名称

		// 将绝对路径"\"替换成"/",文件保存路径
		return savePath.toString().replaceAll("\\\\", "/");
	}
	
	/**
	 * 
	 * @param request 网页请求
	 * @param dir 一级文件目录地址
	 * @param belong 二级文件目录地址名称
	 * @return 
	 */
	public static UploadResult upload(HttpServletRequest request, MultipartFile avatarFile, String dir, String belong) {

		// 获取服务器的实际路径
		String serverSaveDir = getServerSaveDir(request, dir, belong);

		System.out.println("-------服务器的实际路径-----:"+serverSaveDir);

		// 生成文件名称
		String newFileName = rename(avatarFile);

		// 先把用户上传到原图保存到服务器上
		File targetFile = new File(serverSaveDir, newFileName);
		boolean flag = false;
		try {
			// 将文件剪辑并上传到服务器上和本地文件中
			if (!targetFile.exists()) {
				targetFile.mkdirs();
				// 获取文件流,可以进行处理
				InputStream is = avatarFile.getInputStream();
				NewImageUtils.saveImage(is, targetFile);
				// 关闭该流并释放与该流关联的所有系统资源。
				is.close();
				flag = true;
				System.out.println("上传图片成功了............");
			}
		} catch (Exception e) {
			logger.error("UploadFileUtils.Upload:{}", e);
		}
		return new UploadResult(flag, getSavaDir(dir, belong, newFileName));
	}


	/**
	 *
	 * @param cpanpath  C盘路径
	 * @param dir 一级文件目录地址
	 * @param belong 二级文件目录地址名称
	 * @return
	 */
	public static UploadResult uploadCpan(String cpanpath, MultipartFile avatarFile, String dir, String belong) {

		// 获取服务器的实际路径
		String serverSaveDir = getServerSaveDirCpan(cpanpath, dir, belong);

		System.out.println("-------服务器的实际路径-----:"+serverSaveDir);

		// 生成文件名称
		String newFileName = rename(avatarFile);

		// 先把用户上传到原图保存到服务器上
		File targetFile = new File(serverSaveDir, newFileName);
		boolean flag = false;
		try {
			// 将文件剪辑并上传到服务器上和本地文件中
			if (!targetFile.exists()) {
				targetFile.mkdirs();
				// 获取文件流,可以进行处理
				InputStream is = avatarFile.getInputStream();
				NewImageUtils.saveImage(is, targetFile);
				// 关闭该流并释放与该流关联的所有系统资源。
				is.close();
				flag = true;
				System.out.println("上传图片成功了............");
			}
		} catch (Exception e) {
			logger.error("UploadFileUtils.Upload:{}", e);
		}
		return new UploadResult(flag, getSavaDir(dir, belong, newFileName));
	}
	
	/**
	 * 
	 * @param request 网页请求
	 * @param dir 一级文件目录地址
	 * @param belong 二级文件目录地址名称
	 * @return 
	 */
	public static UploadResult upload(HttpServletRequest request, MultipartFile avatarFile, String avatarData,
                                      String dir, String belong) {
		
		// 获取服务器的实际路径
		String serverSaveDir = getServerSaveDir(request, dir, belong);

		// 生成文件名称
		String newFileName = rename(avatarFile);

		// 先把用户上传到原图保存到服务器上
		File targetFile = new File(serverSaveDir, newFileName);
		boolean flag = false;
		try {
			// 创建JSONObject对象
			JSONObject joData = (JSONObject) JSONObject.parse(avatarData);
			// 用户经过剪辑后的图片的大小
			float x = joData.getFloatValue("x");
			float y = joData.getFloatValue("y");
			float w = joData.getFloatValue("width");
			float h = joData.getFloatValue("height");
			float r = joData.getFloatValue("rotate");

			// 将文件剪辑并上传到服务器上和本地文件中
			if (!targetFile.exists()) {
				targetFile.mkdirs();
				// 获取文件流，可以进行处理
				InputStream is = avatarFile.getInputStream();
				// 旋转后剪裁图片
				NewImageUtils.cutAndRotateImage(is, targetFile, (int) x, (int) y, (int) w, (int) h, (int) r);
				// 关闭该流并释放与该流关联的所有系统资源。
				is.close();
				flag = true;
			}
		} catch (Exception e) {
			logger.error("UploadFileUtils.Upload:{}", e);
		}
		return new UploadResult(flag, getSavaDir(dir, belong, newFileName));
	}



	public static void downloadpic(String uploadPath,String tomcatPath, String picImg){
		System.out.println("picImg:"+picImg);
		//将查看图片保存到tomcat目录
		if(picImg.startsWith("/")){  //去掉/
			picImg=picImg.substring(1,picImg.length());
		}
		String ftpfilepath=uploadPath+picImg;
		String tomcatfilepath= tomcatPath+"uploads/"+picImg;  //网站图片时放在uploads目录下
		//文件绝对路径
		//System.out.println("ftpfilepath:"+ftpfilepath);
		//System.out.println("tomcatfilepath:"+tomcatfilepath);
		File downFile = new File(tomcatfilepath);
		if(!downFile.exists()){
			//tomcat文件路径
			try {
				UploadManager.copyFileUsingFileChannels(ftpfilepath,tomcatfilepath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//只有下载详情图片时，不需要加uploads目录，路径已包含---html代码自己寻找
	public static void downDetailpic(String uploadPath,String tomcatPath, String picImg){
		System.out.println("picImg:"+picImg);
		//将查看图片保存到tomcat目录
		if(picImg.startsWith("/")){  //去掉/
			picImg=picImg.substring(1,picImg.length());
		}
		String ftpfilepath=uploadPath+picImg;
		String tomcatfilepath= tomcatPath+picImg;  //网站图片时放在uploads目录下
		//文件绝对路径
		//System.out.println("ftpfilepath:"+ftpfilepath);
		//System.out.println("tomcatfilepath:"+tomcatfilepath);
		File downFile = new File(tomcatfilepath);
		if(!downFile.exists()){
			//tomcat文件路径
			try {
				UploadManager.copyFileUsingFileChannels(ftpfilepath,tomcatfilepath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


}
