package org.pussinboots.morning.os.controller.user;

import org.apache.commons.lang3.StringUtils;
import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.constant.CommonReturnCode;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.order.common.enums.LogisticsComEnum;
import org.pussinboots.morning.order.common.enums.ReturnStatusEnum;
import org.pussinboots.morning.order.pojo.vo.ReturnProductVO;
import org.pussinboots.morning.order.service.IOrderService;
import org.pussinboots.morning.order.service.ReturnProductService;
import org.pussinboots.morning.os.common.result.OsResult;
import org.pussinboots.morning.os.common.security.AuthorizingUser;
import org.pussinboots.morning.os.common.util.NewDateUtils;
import org.pussinboots.morning.os.common.util.SingletonLoginUtils;
import org.pussinboots.morning.user.common.constant.UserReturnCode;
import org.pussinboots.morning.user.entity.Address;
import org.pussinboots.morning.user.entity.Favorit;
import org.pussinboots.morning.user.entity.Message;
import org.pussinboots.morning.user.entity.User;
import org.pussinboots.morning.user.pojo.vo.UserVO;
import org.pussinboots.morning.user.service.IAddressService;
import org.pussinboots.morning.user.service.IFavoritService;
import org.pussinboots.morning.user.service.IMessageService;
import org.pussinboots.morning.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Date;
import java.util.List;


/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：UserInfoController   
* 类描述：后台中心-个人中心表示层控制器   
* 创建人：代远航
* 创建时间：2019年5月9日 下午5:27:30
*
 */
@Controller
@RequestMapping(value = "/uc/user")
@Api(value = "个人中心", description = "个人中心")
public class UserInfoController extends BaseController {
	
	@Autowired
	private IUserService userService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IFavoritService favoritService;
	@Autowired
	private IOrderService orderService;
	@Autowired
	private IMessageService messageService;

	/**
	 * GET 我的个人中心
	 * @return
	 */
	@ApiOperation(value = "我的个人中心", notes = "我的个人中心")  
	@GetMapping(value = "/portal")
	public String portal(Model model) {
		UserVO userVO = userService.getById(SingletonLoginUtils.getUserId());
		String welcomeWord=NewDateUtils.getWellcomeWord();
        Integer waitPayNum=orderService.getOrderCount(userVO.getUserId(),"1",null);
		Integer waitInNum=orderService.getOrderCount(userVO.getUserId(),"4",null);
		Integer waitJudgeNum=orderService.waitJudgeProductCount(userVO.getUserId());
		Favorit favorit=new Favorit();
		favorit.setUserId(userVO.getUserId());
		favorit.setStatus(1);
		Integer favoritNum= favoritService.selectCount(favorit);
		model.addAttribute("favoritNum", favoritNum);
		model.addAttribute("waitPayNum", waitPayNum);
		model.addAttribute("waitInNum", waitInNum);
		model.addAttribute("waitJudgeNum", waitJudgeNum);
		model.addAttribute("welcomeWord", welcomeWord);
		model.addAttribute("userVO", userVO);
		return "/modules/usercenter/user_portal";
	}

	/**
	 * GET 我的信息
	 * @return
	 */
	@ApiOperation(value = "我的信息", notes = "我的信息")
	@GetMapping(value = "/userMess")
	public String personMess(Model model) {
		UserVO userVO = userService.getById(SingletonLoginUtils.getUserId());
		userVO=userService.formatUserVOInfo(userVO);
		model.addAttribute("userVO", userVO);
		return "/modules/usercenter/user_mess";
	}

	/**
	 * GET 我的信息
	 * @return
	 */
	@ApiOperation(value = "完善个人信息", notes = "完善个人信息")
	@GetMapping(value = "/addMess")
	public String addMess(Model model) {
		UserVO userVO = userService.getById(SingletonLoginUtils.getUserId());
		userVO=userService.formatUserVOInfo(userVO);
		System.out.println("省份ID："+userVO.getProvinceId());
		System.out.println("城市ID："+userVO.getCityId());
		model.addAttribute("userVO", userVO);
		return "/modules/usercenter/add_mess";
	}


	/**
	 * POST 完善个人信息
	 * @return
	 */
	@ApiOperation(value = "完善个人信息", notes = "完善个人信息")
	@PostMapping(value = "/saveMess")
	@ResponseBody
	public Object saveMess( @RequestParam("userName") String userName, @RequestParam("realName") String realName,
							@RequestParam("sex") Integer sex,@RequestParam("age") Integer age,
							@RequestParam("provinceName") String provinceName, @RequestParam("cityName") String cityName,
							@RequestParam("provinceId") Integer provinceId,@RequestParam("cityId") Integer cityId) {
		//Long userId=SingletonLoginUtils.getUserId();
		//Integer  count=userService.updatePerfectUser(userId,userName,realName,sex,age);
		AuthorizingUser auser=SingletonLoginUtils.getUser();
		User user=userService.getByLoginName(auser.getTelephone());
		user.setUserName(userName);
		user.setRealName(realName);
		user.setSex(sex);
		user.setAge(age);
		user.setProvinceId(provinceId);
		user.setProvinceName(provinceName);
		user.setCityId(cityId);
		user.setCityName(cityName);
		userService.updateById(user);
		return new OsResult(CommonReturnCode.SUCCESS);
	}

	/**
	 * GET 修改密码
	 * @return
	 */
	@ApiOperation(value = "修改密码", notes = "修改密码")
	@GetMapping(value = "/password")
	public String password(Model model) {
		UserVO userVO = userService.getById(SingletonLoginUtils.getUserId());
		userVO=userService.formatUserVOInfo(userVO);
		model.addAttribute("userVO", userVO);
		return "/modules/usercenter/password";
	}


	/**
	 * POST 完善个人信息
	 * @return
	 */
	@ApiOperation(value = "修改密码", notes = "修改密码")
	@PostMapping(value = "/savePasswd")
	@ResponseBody
	public Object savePasswd( @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword,
			 @RequestParam("randCode") String randCode) {
		if (!SingletonLoginUtils.validate(randCode)) {
			return new OsResult(UserReturnCode.REGISTER_CODE_ERROR);
		}
		AuthorizingUser auser=SingletonLoginUtils.getUser();
		User user=userService.getByLoginName(auser.getTelephone());

		//判断旧密码
		if(!userService.judgeOldPasswordRight(user,oldPassword)){
			return new OsResult(UserReturnCode.WRONG_OLDPASSWORD);
		}
		// 更新账户密码
		Integer count=userService.updatePassword(user,newPassword);
		return new OsResult(CommonReturnCode.SUCCESS,count);
	}


	/**
	 * GET 喜欢的商品
	 * @return
	 */
	@ApiOperation(value = "喜欢的商品", notes = "喜欢的商品")  
	@GetMapping(value = "/favorite")
	public String favorite(Model model,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@RequestParam(value = "limit", required = false, defaultValue = "12") Integer limit) {

		PageInfo pageInfo = new PageInfo(limit, page);
		BasePageDTO<Favorit> basePageDTO = favoritService.listByUserId(SingletonLoginUtils.getUserId(), pageInfo);
		model.addAttribute("favorites", basePageDTO.getList());
		model.addAttribute("pageInfo", basePageDTO.getPageInfo());
		return "/modules/usercenter/user_favorite";
	}
	
	/**
	 * DELETE 喜欢的商品
	 * @return
	 */
	@ApiOperation(value = "删除喜欢的商品", notes = "根据url传过来的商品编号删除喜欢的商品")  
	@DeleteMapping(value = "/favorite/{productNumber}")
	@ResponseBody
	public Object favoriteDelete(@PathVariable("productNumber") Long productNumber) {
		Integer count = favoritService.deleteByProductNumber(SingletonLoginUtils.getUserId(), productNumber);
		return new OsResult(CommonReturnCode.SUCCESS, 1);
	}
	
	/**
	 * GET 收货地址
	 * @return
	 */
	@ApiOperation(value = "收货地址列表", notes = "收货地址列表") 
	@GetMapping(value = "/address")
	public String address(Model model,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@RequestParam(value = "limit", required = false, defaultValue = "11") Integer limit) {
		
		PageInfo pageInfo = new PageInfo(limit, page);
		BasePageDTO<Address> basePageDTO = addressService.listByUserId(SingletonLoginUtils.getUserId(), pageInfo);
		model.addAttribute("addresses", basePageDTO.getList());
		model.addAttribute("pageInfo", basePageDTO.getPageInfo());
		
		return "/modules/usercenter/user_address";
	}
	
	/**
	 * POST 创建收货地址
	 * @return
	 */
	@ApiOperation(value = "创建收货地址", notes = "根据url传过来的收货地址信息创建收货地址")
	@PostMapping(value = "/address")
	@ResponseBody
	public Object addressCreate(Address address) {
		Integer count = addressService.insertAddress(SingletonLoginUtils.getUserId(), address);
		return new OsResult(CommonReturnCode.SUCCESS, count);
	}
	
	/**
	 * PUT 更新收货地址
	 * @return
	 */
	@ApiOperation(value = "更新收货地址", notes = "根据url传过来的收获地址ID和收货地址信息更新收货地址")
	@PutMapping(value = "/address/{addressId}")
	@ResponseBody
	public Object addressUpdate(Address address, @PathVariable("addressId") Long addressId) {
		address.setAddressId(addressId);
		Integer count = addressService.updateAddress(SingletonLoginUtils.getUserId(), address);
		return new OsResult(CommonReturnCode.SUCCESS, count);
	}
	
	/**
	 * DELETE 收货地址
	 * @return
	 */
	@ApiOperation(value = "删除收货地址", notes = "根据url传过来的地址ID删除收货地址")  
	@DeleteMapping(value = "/address/{addressId}")
	@ResponseBody
	public Object addressDelete(@PathVariable("addressId") Long addressId) {
		Integer count = addressService.deleteByAddressId(SingletonLoginUtils.getUserId(), addressId);
		return new OsResult(CommonReturnCode.SUCCESS, count);
	}


	/**
	 * GET 通知消息
	 * @param model
	 * @return
	 */
	@ApiOperation(value = "通知消息", notes = "通知消息")
	@GetMapping(value = "/msglist")
	public String msglist(Model model, @RequestParam(value = "page", required = false, defaultValue = "1") String reqPage,
							  @RequestParam(value = "limit", required = false, defaultValue = "6") Integer limit) {

		// 请求参数:分页,如果分页不存在或者不为Integer类型,则默认1/默认页数
		Integer page = StringUtils.isNumeric(reqPage) ? Integer.valueOf(reqPage) : 1;
		PageInfo pageInfo = new PageInfo(limit, page);

		BasePageDTO<Message> basePageDTO=messageService.getMessagelist(SingletonLoginUtils.getUserId(), pageInfo);
		List<Message> vlist=basePageDTO.getList();

		model.addAttribute("messageVOs", vlist);     // 退货列表
		model.addAttribute("pageInfo", basePageDTO.getPageInfo()); // 分页信息
		System.out.println("获取了信息");
		return "/modules/usercenter/user_message";
	}


	/**
	 * GET 通知消息
	 * @param model
	 * @return
	 */
	@ApiOperation(value = "意见反馈", notes = "意见反馈")
	@GetMapping(value = "/suggestion")
	public String suggestion(Model model) {
		return "/modules/usercenter/suggestion";
	}



	/**
	 * POST 保存意见
	 * @return
	 */
	@ApiOperation(value = "保存意见", notes = "保存意见")
	@PostMapping(value = "/saveSuggest")
	@ResponseBody
	public Object saveSuggest( @RequestParam("content") String content,
							  @RequestParam("randCode") String randCode) {
		if (!SingletonLoginUtils.validate(randCode)) {
			return new OsResult(UserReturnCode.REGISTER_CODE_ERROR);
		}
		AuthorizingUser auser=SingletonLoginUtils.getUser();
		User user=userService.getByLoginName(auser.getTelephone());

		Message message=new Message();
		message.setUserId(9999999999L);
		message.setUserName("系统管理员");
		message.setContent(content);
		message.setType(6);
		message.setStatus(0);
		message.setSendUserId(user.getUserId());
		message.setSendUserName(user.getUserName());
		message.setCreateTime(new Date());
		// 更新账户密码
		messageService.insert(message);
		return new OsResult(CommonReturnCode.SUCCESS,1);
	}



}
