package org.pussinboots.morning.os.service.impl;
import org.pussinboots.morning.os.common.sms.PuXunSmsUtils;
import org.pussinboots.morning.os.common.util.StringUtils;
import org.pussinboots.morning.os.service.SmsService;
import org.springframework.stereotype.Service;

@Service
public class SmsServiceImpl implements SmsService {

	@Override
	public void sendSMS(String mobile, String content) {
		//System.out.println("---发送短信测试--sendSMS--1");
		if(mobile==null || "".equals(mobile.trim()) || content==null || "".equals(content.trim())){
			return;
		}
		mobile=mobile.trim();
		content=content.trim();
		if(!mobile.startsWith("1") || mobile.length()!=11 ){
			return;
		}
		if(StringUtils.isInteger(mobile)){  //全是数字---是手机号码
			//普讯短信供应商
			//System.out.println("---发送短信-----");
			PuXunSmsUtils.sendSMS(mobile,content);
			//其它短信供应商
			//..............
		}
	}
}
