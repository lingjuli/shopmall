package org.pussinboots.morning.os.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.commons.lang3.StringUtils;
import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.constant.CommonReturnCode;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.order.common.enums.LogisticsComEnum;
import org.pussinboots.morning.order.common.enums.OrderTypeEnum;
import org.pussinboots.morning.order.common.enums.ReturnProductEnum;
import org.pussinboots.morning.order.common.enums.ReturnStatusEnum;
import org.pussinboots.morning.order.entity.Order;
import org.pussinboots.morning.order.entity.OrderProduct;
import org.pussinboots.morning.order.entity.OrderStatus;
import org.pussinboots.morning.order.entity.ReturnProductDO;
import org.pussinboots.morning.order.pojo.vo.OrderVO;
import org.pussinboots.morning.order.pojo.vo.OrdercVO;
import org.pussinboots.morning.order.pojo.vo.ReturnProductVO;
import org.pussinboots.morning.order.service.IOrderProductService;
import org.pussinboots.morning.order.service.IOrderService;
import org.pussinboots.morning.order.service.ReturnProductService;
import org.pussinboots.morning.os.common.result.OsResult;
import org.pussinboots.morning.os.common.security.AuthorizingUser;
import org.pussinboots.morning.os.common.util.NewDateUtils;
import org.pussinboots.morning.os.common.util.SingletonLoginUtils;
import org.pussinboots.morning.product.entity.Comment;
import org.pussinboots.morning.product.entity.ProductAttribute;
import org.pussinboots.morning.product.entity.ProductSpecification;
import org.pussinboots.morning.product.pojo.vo.ProductVO;
import org.pussinboots.morning.product.service.ICommentService;
import org.pussinboots.morning.product.service.IProductAttributeService;
import org.pussinboots.morning.product.service.IProductService;
import org.pussinboots.morning.product.service.IProductSpecificationService;
import org.pussinboots.morning.user.entity.User;
import org.pussinboots.morning.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：UserOrderController   
* 类描述：后台中心-订单中心表示层控制器      
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:21:35
*
 */
@Controller
@RequestMapping(value = "/uc/order")
@Api(value = "订单中心", description = "订单中心")
public class UserOrderController extends BaseController {
	
	@Autowired
	private IOrderService orderService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ICommentService commentService;
	@Autowired
	private IOrderProductService orderProductService;
	@Autowired
	private IProductService productService;
	@Autowired
	private IProductAttributeService productAttributeService;
	@Autowired
	private IProductSpecificationService productSpecificationService;
	@Autowired
	private ReturnProductService returnProductService;


	/**
	 * GET 我的订单
	 * @param model
	 * @return
	 */
	@ApiOperation(value = "我的订单", notes = "我的订单")  
	@GetMapping(value = "/list")
	public String list(Model model, @RequestParam(value = "page", required = false, defaultValue = "1") String reqPage,
			@RequestParam(value = "type", required = false, defaultValue = "0") String reqSort,
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "limit", required = false, defaultValue = "6") Integer limit) {
		
		
		// 请求参数:排序方式,如果排序方式不存在或者不为Integer类型,则默认0/全部有效订单
		Integer type = StringUtils.isNumeric(reqSort) ? Integer.valueOf(reqSort) : OrderTypeEnum.ALL_VALID.getType();
		// 请求参数:分页,如果分页不存在或者不为Integer类型,则默认1/默认页数
		Integer page = StringUtils.isNumeric(reqPage) ? Integer.valueOf(reqPage) : 1;
		
		PageInfo pageInfo = new PageInfo(limit, page);
		
		BasePageDTO<OrderVO> basePageDTO = orderService.list(SingletonLoginUtils.getUserId(), pageInfo,
				OrderTypeEnum.typeOf(type).getTypeValue(), search);	
		
		model.addAttribute("orderVOs", basePageDTO.getList()); // 订单列表
		model.addAttribute("pageInfo", basePageDTO.getPageInfo()); // 分页信息
		model.addAttribute("type", type); // 排序方式
		model.addAttribute("search", search);// 搜索内容
		
		return "/modules/usercenter/user_order";
	}
	
	/**
	 * GET 订单详情
	 * @param model
	 * @param orderNumber
	 * @return
	 */
	@ApiOperation(value = "订单详情", notes = "根据URL传过来的订单编号获取订单详情信息")  
	@GetMapping(value = "/{orderNumber}")
	public String orderView(Model model, @PathVariable("orderNumber") Long orderNumber) {
		OrderVO orderVO = orderService.getOrder(SingletonLoginUtils.getUserId(), orderNumber);
		int  canReturnProduct=0;  //0-不显示可退货  1-显示可退货
		if(orderVO.getOrderStatus()==5 || orderVO.getOrderStatus()==6){
			List<OrderStatus> oslist=orderVO.getOrderStatusList();
			for(OrderStatus os: oslist){
				if(os.getOrderStatus()==5){  //已收货7天后不可退款
					int days= NewDateUtils.getDiffDays(os.getCreateTime(),new Date());
					if(days<7){
						canReturnProduct=1;
					}
					break;
				}
			}
		}
		List<OrderProduct> oplist=orderVO.getOrderProducts();
		for(OrderProduct op: oplist){
            if(op.getReturnStatus()!=null && op.getReturnStatus()!=0){
				op.setReturnStatusStr(ReturnStatusEnum.stateOf(op.getReturnStatus()).getStateInfo());
			}else{
				op.setReturnStatusStr("");
			}
		}
		if(canReturnProduct==1){ //进一步判断，如果全部商品已退货，不显示
			int flag=0;  //0-全部以退货  0-有商品未退货
			for(OrderProduct op: oplist){
				if(op.getReturnStatus()==null || op.getReturnStatus()==0){
					flag=1;
					break;
				}
			}
			if(flag==0){
				canReturnProduct=0;
			}
		}
		model.addAttribute("canReturnProduct", canReturnProduct);
		model.addAttribute("orderVO", orderVO);
		return "/modules/usercenter/user_order_view";
	}




	/**
	 * POST 申请退货
	 * @return
	 */
	@ApiOperation(value = "申请退货", notes = "申请退货")
	@GetMapping(value = "/returnProduct/{orderNumber}")
	public String returnProduct(Model model, @PathVariable(value = "orderNumber", required = true) Long orderNumber) {
		OrderVO orderVO = orderService.getOrder(SingletonLoginUtils.getUserId(), orderNumber);
		List<OrderProduct> newoplist=new ArrayList<OrderProduct>();
		List<OrderProduct> oplist=orderVO.getOrderProducts();
		for(OrderProduct op: oplist){
			if(op.getReturnStatus()==null || op.getReturnStatus()==0){
				newoplist.add(op);
			}
		}
		orderVO.setOrderProducts(newoplist);

		ReturnProductEnum[] returnReasons=ReturnProductEnum.values();
		model.addAttribute("returnReasons", returnReasons);
		model.addAttribute("orderVO", orderVO);
		return  "/modules/usercenter/user_return_order";
	}


	/**
	 * POST 申请退货
	 * @return
	 */
	@ApiOperation(value = "申请退货", notes = "申请退货")
	@PostMapping(value = "/returnProductCommit")
	@ResponseBody
	public Object returnProductCommit(
			@RequestParam("orderId") Long orderId,
			@RequestParam("orderNumber") Long orderNumber,
			@RequestParam("orderProductIds") String orderProductIds,
			@RequestParam("returnReason") Integer returnReason,
			@RequestParam("questionContent") String questionContent) {
		System.out.println("orderId"+orderId);
		System.out.println("orderNumber"+orderNumber);
		System.out.println("orderProductIds"+orderProductIds);
		System.out.println("returnReason"+returnReason);
		System.out.println("questionContent"+questionContent);
		AuthorizingUser user=SingletonLoginUtils.getUser();
		ReturnProductDO  rp=new ReturnProductDO();
		rp.setUserId(user.getUserId());
		rp.setUserName(user.getUserName());
		rp.setTelehpone(user.getTelephone());
		rp.setOrderId(orderId);
		rp.setOrderNumber(orderNumber);
		String ids=orderProductIds.substring(0,orderProductIds.length()-1);
		rp.setOrderProductIds(ids);
		rp.setReturnReason(returnReason);
		rp.setQuestionRemark(questionContent);
		rp.setCreateTime(new Date());
		rp.setReturnStatus(1);
		returnProductService.save(rp);

		String[] opidArr=ids.split(",");
		for(String opid: opidArr){
				OrderProduct op=orderProductService.selectById(Long.parseLong(opid));
				op.setReturnStatus(1);
				orderProductService.updateById(op);
		}
		return new OsResult(CommonReturnCode.SUCCESS, 1);
	}

	/**
	 * POST 申请退款
	 * @return
	 */
	@ApiOperation(value = "申请退款", notes = "申请退款")
	@GetMapping(value = "/returnMoneyOrder/{orderNumber}")
	@ResponseBody
	public Object returnMoneyOrder(@PathVariable(value = "orderNumber", required = true) Long orderNumber) {
		Integer count=orderService.returnMoneyOrder(orderNumber,SingletonLoginUtils.getUserId());
		if(count>0){
			return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
		}else{
			return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),CommonReturnCode.UNKNOWN_ERROR.getMessage());
		}
	}

	/**
	 * POST 配货订单
	 * @return
	 */
	@ApiOperation(value = "配货订单", notes = "配货订单")
	@GetMapping(value = "/pickingOrder/{orderNumber}")
	@ResponseBody
	public Object pickingOrder(@PathVariable(value = "orderNumber", required = true) Long orderNumber) {
		Integer count=orderService.pickingOrder(orderNumber,SingletonLoginUtils.getUserId());
		if(count>0){
			return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
		}else{
			return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),CommonReturnCode.UNKNOWN_ERROR.getMessage());
		}
	}

	/**
	 * POST 订单发货
	 * @return
	 */
	@ApiOperation(value = "订单发货", notes = "订单发货")
	@GetMapping(value = "/sendOrder/{orderNumber}")
	@ResponseBody
	public Object sendOrder(@PathVariable(value = "orderNumber", required = true) Long orderNumber) {
		Integer count=orderService.sendOrder(orderNumber,SingletonLoginUtils.getUserId());
		if(count>0){
			return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
		}else{
			return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),CommonReturnCode.UNKNOWN_ERROR.getMessage());
		}
	}

	/**
	 * POST 确认收货
	 * @return
	 */
	@ApiOperation(value = "确认收货", notes = "确认收货")
	@GetMapping(value = "/inOrder/{orderNumber}")
	@ResponseBody
	public Object inOrder(@PathVariable(value = "orderNumber", required = true) Long orderNumber) {
		try{
			Order order=orderService.inOrder(orderNumber,SingletonLoginUtils.getUserId());
			//更新用户的消费额和用户积分
			AuthorizingUser auser=SingletonLoginUtils.getUser();
			User user=userService.getByLoginName(auser.getTelephone());
			BigDecimal newAmt=user.getAmount().add(order.getPayAmount());
			user.setAmount(newAmt);
			Integer newScore=user.getScore()+order.getOrderScore();
			user.setScore(newScore);
			userService.updateById(user);
			return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
		}catch (Exception e){
			return new OsResult(CommonReturnCode.UNKNOWN_ERROR.getCode(),CommonReturnCode.UNKNOWN_ERROR.getMessage());
		}

	}



	/**
	 * GET 我的订单
	 * @param model
	 * @return
	 */
	@ApiOperation(value = "评价订单", notes = "评价订单")
	@GetMapping(value = "/judgelist")
	public String judgelist(Model model, @RequestParam(value = "page", required = false, defaultValue = "1") String reqPage,
					   @RequestParam(value = "type", required = false, defaultValue = "0") String reqSort,
					   @RequestParam(value = "search", required = false) String search,
					   @RequestParam(value = "limit", required = false, defaultValue = "6") Integer limit) {


		// 请求参数:排序方式,如果排序方式不存在或者不为Integer类型,则默认0/全部有效订单
		Integer type = StringUtils.isNumeric(reqSort) ? Integer.valueOf(reqSort) : 0;
		String orderStatus="5";
		if(type!=0 && type!=1 && type!=2){
			type=0;
		}
		if(type==1){
			orderStatus="6";
		}else if(type==2){
			orderStatus="5,6";
		}

		// 请求参数:分页,如果分页不存在或者不为Integer类型,则默认1/默认页数
		Integer page = StringUtils.isNumeric(reqPage) ? Integer.valueOf(reqPage) : 1;

		PageInfo pageInfo = new PageInfo(limit, page);

		BasePageDTO<OrdercVO> basePageDTO = orderService.listc(SingletonLoginUtils.getUserId(), pageInfo,
				orderStatus, search);  //已完成订单

		model.addAttribute("orderVOs", basePageDTO.getList()); // 订单列表
		model.addAttribute("pageInfo", basePageDTO.getPageInfo()); // 分页信息
		model.addAttribute("type", type); // 排序方式
		model.addAttribute("search", search);// 搜索内容

		return "/modules/usercenter/judge_order";
	}


	/**
	 * GET 服务单
	 * @param model
	 * @return
	 */
	@ApiOperation(value = "服务单", notes = "服务单")
	@GetMapping(value = "/servicelist")
	public String servicelist(Model model, @RequestParam(value = "page", required = false, defaultValue = "1") String reqPage,
							@RequestParam(value = "limit", required = false, defaultValue = "6") Integer limit) {

		// 请求参数:分页,如果分页不存在或者不为Integer类型,则默认1/默认页数
		Integer page = StringUtils.isNumeric(reqPage) ? Integer.valueOf(reqPage) : 1;

		PageInfo pageInfo = new PageInfo(limit, page);

		BasePageDTO<ReturnProductVO> basePageDTO=returnProductService.returnlist(SingletonLoginUtils.getUserId(), pageInfo);
//		BasePageDTO<OrdercVO> basePageDTO = orderService.listc(SingletonLoginUtils.getUserId(), pageInfo,
//				orderStatus, search);  //已完成订单
		List<ReturnProductVO>  vlist=basePageDTO.getList();
		for(ReturnProductVO v:  vlist){
			v.setReturnStatusStr(ReturnStatusEnum.stateOf(v.getReturnStatus()).getStateInfo());
		}
		LogisticsComEnum[]  logisticsComList= LogisticsComEnum.values();
		model.addAttribute("logisticsComList", logisticsComList);
		model.addAttribute("returnVOs", vlist);     // 退货列表
		model.addAttribute("pageInfo", basePageDTO.getPageInfo()); // 分页信息

		return "/modules/usercenter/service_order";
	}

	/**
	 * GET 服务单详情
	 * @param model
	 * @param returnId
	 * @return
	 */
	@ApiOperation(value = "订单详情", notes = "根据URL传过来的订单编号获取订单详情信息")
	@GetMapping(value = "/svcdetail/{returnId}")
	public String svcdetail(Model model, @PathVariable("returnId") Long returnId) {
		ReturnProductVO returnVO=  returnProductService.returnOne(returnId);
		if(returnVO.getUserId().longValue()!=SingletonLoginUtils.getUserId().longValue()){
			return "/modules/usercenter/svc_order_view";
		}
		returnVO.setReturnStatusStr(ReturnStatusEnum.stateOf(returnVO.getReturnStatus()).getStateInfo());
		List<OrderProduct> oplist=returnVO.getOrderProducts();
		BigDecimal  productTotalAmt=new BigDecimal(0);
		for(OrderProduct op: oplist){
			productTotalAmt=productTotalAmt.add(op.getProductAmount());
		}
		String  returnReasonStr=ReturnProductEnum.stateOf(returnVO.getReturnReason()).getStateInfo();
		model.addAttribute("returnReasonStr", returnReasonStr);
		model.addAttribute("productTotalAmt", productTotalAmt);
		model.addAttribute("returnVO", returnVO);
		return "/modules/usercenter/svc_order_view";
	}


	@ApiOperation(value = "提交物流信息", notes = "提交物流信息")
	@PostMapping(value = "/commitServiceInfo")
	@ResponseBody
	public Object commitServiceInfo(@RequestParam("returnId") Long returnId,
									@RequestParam("logisticsCom") Integer logisticsCom,
									@RequestParam("logisticsNo") String logisticsNo) {
		AuthorizingUser auser=SingletonLoginUtils.getUser();
		User user=userService.getByLoginName(auser.getTelephone());
		ReturnProductDO returnProduct=returnProductService.get(returnId);
		if(returnProduct.getReturnStatus()==2){
			String logisticsComStr=LogisticsComEnum.stateOf(logisticsCom).getStateInfo();
			returnProduct.setReturnStatus(3);
			returnProduct.setLogisticsCom(logisticsComStr);
			returnProduct.setLogisticsNo(logisticsNo);
			returnProductService.update(returnProduct);
			//更新商品状态
			String[]  ids=returnProduct.getOrderProductIds().split(",");
			for(String id:  ids){
				OrderProduct   op=	orderProductService.selectById(Long.parseLong(id));
				op.setReturnStatus(3);
				orderProductService.updateById(op);
			}
		}
		return new OsResult(CommonReturnCode.SUCCESS);
	}


	@ApiOperation(value = "评价商品", notes = "评价商品")
	@PostMapping(value = "/judgeOrderProduct")
	@ResponseBody
	public Object judgeOrderProduct(@RequestParam("orderId") Long orderId,
									@RequestParam("orderNumber") Long orderNumber,
								    @RequestParam("productNumber") Long productNumber,
									@RequestParam("productSpecNumber") Long productSpecNumber,
									@RequestParam("star") Integer star,
									@RequestParam("judgeContent") String judgeContent) {
		AuthorizingUser auser=SingletonLoginUtils.getUser();
		User user=userService.getByLoginName(auser.getTelephone());
		ProductVO pv=productService.getByNumber(productNumber,1);
        String specName="";  //获取规格名称
		ProductSpecification ps=productSpecificationService.getProductSpecification(productSpecNumber);
		specName=ps.getSpecName();
		System.out.println("评论时获取的规格名称："+specName);
		//保存评价信息
		Comment  comment=new Comment();
		comment.setProductId(pv.getProductId());
		comment.setProductNumber(productSpecNumber);
		comment.setUserId(user.getUserId());
		comment.setUserName(user.getUserName());
		comment.setPicImg(user.getPicImg());
		comment.setOrderId(orderId);
		comment.setStar(star);
		comment.setContent(judgeContent);
		comment.setGoodCount(0);
		comment.setStatus(0);  //1-显示 0-隐藏
		comment.setType(0); // 1-优质 0-普通
		comment.setCreateTime(new Date());
		comment.setCreateBy(user.getUserName());
		comment.setUpdateTime(new Date());
		comment.setUpdateBy(user.getUserName());
		comment.setSpecName(specName);
		comment.setProdNumber(pv.getProductNumber());
		comment.setTelephone(user.getTelephone());
		comment.setBrandId(pv.getBrandId());
		commentService.insert(comment);
        //更新商品评价状态
		List<OrderProduct> oplist=orderProductService.listByOrderId(orderId);
		for(OrderProduct op: oplist){
			if(op.getProductSpecNumber().longValue()==productSpecNumber.longValue()){
				op.setCommentStatus(1);  //已评价
				orderProductService.updateById(op);
				break;
			}
		}
		//如果所有商品都评价，更新订单状态为已评价
		String flag="1";
		for(OrderProduct op: oplist){
			if(op.getCommentStatus()==0){
				flag="0";
				break;
			}
		}
		if("1".equals(flag)){
			orderService.judgeOrder(orderNumber,auser.getUserId());
		}

		// 商品评价总数(非显示评价数)加1
		ProductAttribute productAttribute = productAttributeService.getByProductId(pv.getProductId());
		productAttribute.setCommentTotal(productAttribute.getCommentTotal()+1);
		productAttributeService.updateById(productAttribute);

		return new OsResult(CommonReturnCode.SUCCESS, orderNumber.toString());
	}
}
