package org.pussinboots.morning.os.controller.webfront;

import java.util.List;

import org.pussinboots.morning.common.base.BaseController;
import org.pussinboots.morning.online.common.enums.AdvertTypeEnum;
import org.pussinboots.morning.online.common.enums.NavigationBarTypeEnum;
import org.pussinboots.morning.online.entity.AdvertDetail;
import org.pussinboots.morning.online.entity.NavigationBar;
import org.pussinboots.morning.online.service.IAdvertDetailService;
import org.pussinboots.morning.online.service.INavigationBarService;
import org.pussinboots.morning.os.common.upload.UploadManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;

/**
 * 
* 项目名称：morning-os-web Maven Webapp   
* 类名称：WebFrontController   
* 类描述：商城首页表示层控制器       
* 创建人：代远航
* 创建时间：2019年4月9日 下午4:59:20
*
 */
@Controller
@Api(value = "商城首页", description = "商城首页")
public class WebFrontController extends BaseController {
	
	@Autowired
	private IAdvertDetailService advertDetailService;
	@Autowired
	private INavigationBarService navigationBarService;


	@GetMapping({ "/", "" })
	String welcome(Model model) {
		//访问网站项目时自动定位到首页
		return "redirect:/index";
	}
	
	/**
	 * GET 商城首页
	 * @return
	 */
	@ApiOperation(value = "商城首页", notes = "商城首页展示页面")  
	@GetMapping(value = "/index")
	public String index(Model model, HttpServletRequest request) {
		
		// 首页-广告栏-左部导航栏
		List<NavigationBar> indexAdvertLeft = navigationBarService
				.listByNavigationId(NavigationBarTypeEnum.INDEX_ADVERT_LEFT.getType());
		model.addAttribute("indexAdvertLeft", indexAdvertLeft);

		// 首页轮播广告列表
		List<AdvertDetail> indexCarouselImgs = advertDetailService
				.listByAdvertId(AdvertTypeEnum.INDEX_CAROUSEL.getType());
		for(AdvertDetail advert: indexCarouselImgs){
            if(advert.getPicImg()!=null && !"".equals(advert.getPicImg())){
                UploadManager.downloadpic(UploadManager.uploadPath,request.getSession().getServletContext().getRealPath("/"),advert.getPicImg());
            }

        }
		model.addAttribute("indexCarouselImgs", indexCarouselImgs);

		// 首页热点广告列表
		List<AdvertDetail> indexHotAdvertImgs = advertDetailService
				.listByAdvertId(AdvertTypeEnum.INDEX_HOT_ADVERT.getType());
		for(AdvertDetail advert: indexHotAdvertImgs){
			if(advert.getPicImg()!=null && !"".equals(advert.getPicImg())){
				UploadManager.downloadpic(UploadManager.uploadPath,request.getSession().getServletContext().getRealPath("/"),advert.getPicImg());
			}

		}

		model.addAttribute("indexHotAdvertImgs", indexHotAdvertImgs);
		System.out.println("java----代码执行完成");
		return "/modules/webfront/index";
	}


	/**
	 * GET 关于我们
	 * @return
	 */
	@ApiOperation(value = "关于我们", notes = "关于我们")
	@GetMapping(value = "/about")
	public String about(Model model) {
		return "/modules/common/about";
	}

	/**
	 * GET 购物指南
	 * @return
	 */
	@ApiOperation(value = "购物指南", notes = "购物指南")
	@GetMapping(value = "/guide")
	public String guide(Model model) {
		return "/modules/common/guide";
	}



}
