<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>评价晒单 -</title>
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-list-box">
      <div class="box-hd">
        <h1 class="title">消息通知</h1>
        <div class="more clearfix">
        </div>
      </div>
      <div class="box-bd">
        <div id="J_orderList">
          <ul class="order-list">
            <c:forEach items="${messageVOs}" var="messageVO">
                <li class="uc-order-item uc-order-item-shipping">
                  <div class="order-detail">
                    <div class="order-summary">
                      <div class="order-status">
                        <c:if test="${messageVO.type eq 1}">交易物流</c:if>
                        <c:if test="${messageVO.type eq 2}">系统消息</c:if>
                        <c:if test="${messageVO.type eq 3}">特惠活动</c:if>
                      </div>
                    </div>
                    <table class="order-detail-table">
                      <thead>
                      <tr>
                        <th class="col-main"> <p class="caption-info">
                          <fmt:formatDate value="${messageVO.createTime}" pattern="yyyy年MM月dd日 HH:mm" />
                          <span class="sep">|</span> ${messageVO.content} <%-- <a href="${ctx}/uc/order/${orderVO.orderNumber}">${orderVO.orderNumber}</a>--%></p>
                        </th>
                        <th class="col-sub">
                         &nbsp; <%--<p class="caption-price">订单金额：<span class="num">${orderVO.payAmount}</span>元</p>--%>
                        </th>
                      </tr>
                      </thead>
                    </table>
                  </div>
                </li>

            </c:forEach>
          </ul>
        </div>
        <div id="J_orderListPages">
          <c:if test="${pageInfo.total gt pageInfo.limit and not empty messageVOs}">
            <div id="pager" data-pager-href="${ctx}/uc/user/msglist?page=" data-pager-totalPage="${pageInfo.totalPage}" data-pager-nowpage="${pageInfo.current}" data-pager-total="${pageInfo.total}"></div>
          </c:if>
        </div>
      </div>
    </div>
  </div>
</div>

<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script> 
  <!-- 分页js --> 
  <script src="${ctxsta}/common/pager/jquery.pager.js"></script>
  <script type="text/javascript">
		var pagecount = $('#pager').attr('data-pager-totalPage'); // 总页面数
		var nowpage = $('#pager').attr('data-pager-nowpage'); // 当前页数
		var href = $('#pager').attr('data-pager-href'); // 链接地址
		$(document).ready(function() {
			$("#pager").pager({
				pagenumber : nowpage,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
		});
		PageClick = function(number) {
			$("#pager").pager({
				pagenumber : number,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
			window.location.href = href + number;
  	}
  </script>


</myfooter>
</body>
</html>