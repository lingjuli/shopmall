<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>评价晒单 -</title>
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-list-box">
      <div class="box-hd">
        <h1 class="title">评价晒单</h1>
        <div class="more clearfix">
          <ul class="filter-list J_orderType">
            <li class="first ${type eq 0 ? 'active':''}"><a href="${ctx}/uc/order/judgelist?type=0" data-type="0">待评价商品</a></li>
            <li class="${type eq 1 ? 'active':''}"><a id="J_unpaidTab" href="${ctx}/uc/order/judgelist?type=1" data-type="1">已评价商品</a></li>
            </ul>
          <form id="J_orderSearchForm" class="search-form clearfix" action="" method="get">
            <label for="search" class="hide">站内搜索</label>
            <input class="search-text" type="search" id="J_orderSearchKeywords" name="search" value="${search}" autocomplete="off" placeholder="输入商品名称、商品编号、订单号" />
            <input type="hidden" name="type" value="2" />
            <input type="submit" class="search-btn iconfont" value="搜索" />
          </form>
        </div>
      </div>
      <div class="box-bd">
        <div id="J_orderList" >
          <div class="loading hide">
            <div class="loader"></div>
          </div>
        </div>
        <div id="J_orderList">
          <ul class="order-list">
            <c:forEach items="${orderVOs}" var="orderVO">
              <c:if test="${orderVO.orderStatus eq 5}">
                <li class="uc-order-item uc-order-item-pay">
                  <div class="order-detail">
                    <div class="order-summary">
                      <div class="order-status">待评价</div>
                      <%--<p class="order-desc J_deliverDesc"> 评价商品获积分 </p>--%>
                    </div>
                    <table class="order-detail-table">
                      <thead>
                        <tr>
                          <th class="col-main"> <p class="caption-info">
                              <fmt:formatDate value="${orderVO.createTime}" pattern="yyyy年MM月dd日 HH:mm" />
                              <span class="sep">|</span>订单号：${orderVO.orderNumber} <!--a href="$-{ctx}/uc/order/$-{orderVO.orderNumber}"></a--></p>
                          </th>
                          <th class="col-sub">
                            &nbsp;
                            <!--p class="caption-price">订单金额：<span class="num">$-{orderVO.payAmount}</span>元</p-->
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="order-items" colspan="2">
                            <ul class="goods-list">
                              <c:forEach items="${orderVO.orderProducts}" var="orderProduct">
                                <li>
                                  <div class="figure figure-thumb"> <a href="${ctx}/detail/${orderProduct.productNumber}" target="_blank"> <img src="${ctximg}/${orderProduct.picImg}" width="80" height="80" alt="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}" title="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}"> </a> </div>
                                  <p class="name"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}">${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}</a> </p>
                                  <p class="price">${orderProduct.price}元 × ${orderProduct.buyNumber}</p>
                                  <c:if test="${orderProduct.commentStatus eq 0}">
                                    <a class="btn btn-small btn-primary judgeGoods" href="javascript:judgeGoods('${orderVO.orderId}', '${orderVO.orderNumber}','${orderProduct.productNumber}','${orderProduct.productSpecNumber}','${orderProduct.name}');"   >评价商品</a>
                                  </c:if>
                                  <c:if test="${orderProduct.commentStatus eq 1}">
                                    <p >评价信息：${orderProduct.star}分&nbsp;&nbsp;<fmt:formatDate value="${orderProduct.judgetime}" pattern="yyyy年MM月dd日 HH:mm" /> &nbsp;&nbsp;${orderProduct.content}</p>
                                  </c:if>
                                </li>
                              </c:forEach>
                            </ul></td>
                          <td class="order-actions">
                            &nbsp;
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </li>
              </c:if>
              <c:if test="${orderVO.orderStatus eq 6}">
                <li class="uc-order-item uc-order-item-shipping">
                  <div class="order-detail">
                    <div class="order-summary">
                      <div class="order-status">已评价</div>
                      <%--<p class="order-desc J_deliverDesc"> 评价商品获积分 </p>--%>
                    </div>
                    <table class="order-detail-table">
                      <thead>
                      <tr>
                        <th class="col-main"> <p class="caption-info">
                          <fmt:formatDate value="${orderVO.createTime}" pattern="yyyy年MM月dd日 HH:mm" />
                          <span class="sep">|</span>订单号： ${orderVO.orderNumber} <%-- <a href="${ctx}/uc/order/${orderVO.orderNumber}">${orderVO.orderNumber}</a>--%></p>
                        </th>
                        <th class="col-sub">
                         &nbsp; <%--<p class="caption-price">订单金额：<span class="num">${orderVO.payAmount}</span>元</p>--%>
                        </th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td class="order-items"><ul class="goods-list">
                          <c:forEach items="${orderVO.orderProducts}" var="orderProduct">
                            <li>
                              <div class="figure figure-thumb"> <a href="${ctx}/detail/${orderProduct.productNumber}" target="_blank"> <img src="${ctximg}/${orderProduct.picImg}" width="80" height="80" alt="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}" title="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}"> </a> </div>
                              <p class="name"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}">${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}</a> </p>
                              <p class="price">${orderProduct.price}元 × ${orderProduct.buyNumber}</p>
                              <p >评价信息：${orderProduct.star}分&nbsp;&nbsp;<fmt:formatDate value="${orderProduct.judgetime}" pattern="yyyy年MM月dd日 HH:mm" /> &nbsp;&nbsp;${orderProduct.content}</p>
                            </li>
                          </c:forEach>
                        </ul></td>
                        <td class="order-actions">
                          &nbsp; <%--<a class="btn btn-small btn-line-gray" href="${ctx}/uc/order/${orderVO.orderNumber}">订单详情</a>--%>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </li>
              </c:if>
            </c:forEach>
          </ul>
        </div>
        <div id="J_orderListPages">
          <c:if test="${pageInfo.total gt pageInfo.limit and not empty orderVOs}">
            <div id="pager" data-pager-href="${ctx}/uc/order/judgelist?type=${type}&search=${search}&page=" data-pager-totalPage="${pageInfo.totalPage}" data-pager-nowpage="${pageInfo.current}" data-pager-total="${pageInfo.total}"></div>
          </c:if>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="J_judgeGoodsDiv" class="modal modal-verify modal-hide in disabled" aria-hidden="false" style=" border:1px solid #ff6700;display: none;" >
  <div class="modal-hd">
    <h3 class="title">评价商品</h3>
    <a class="close" data-dismiss="modal" href="javascript: void(0);"><i class="glyphicon glyphicon-remove"></i></a> </div>
  <div class="modal-bd">
    <form id="J_judgeForm" class="form-update-addr" method="post" action="">
      <div class="form-row clearfix" style="width: 100%">
        <fieldset class="form-section form-section-active">
          <label class="input-label" >评价商品</label>
          <div  style="width: 100%;">
            <br>
            <input type="text" id="goodName" value="" size="70"  readonly>
          </div>

         </fieldset>
      </div>
      <div class="form-row clearfix">
        <fieldset class="form-section form-section-active">
          <label class="input-label" >商品评价</label>
          <input type="hidden" name="orderId" id="orderId" value="">
          <input type="hidden" name="orderNumber" id="orderNumber" value="">
          <input type="hidden" name="productNumber" id="productNumber" value="">
          <input type="hidden" name="productSpecNumber" id="productSpecNumber" value="">
          <div  style="width: 100%;">
           <br/>
           <label class="radio-inline">
             <input type="radio" name="star" id="star1" value="1"> 很不满意
           </label>
           <label class="radio-inline">
             <input type="radio" name="star" id="star2" value="2"> 不满意
           </label>
           <label class="radio-inline">
             <input type="radio" name="star" id="star3" value="3"> 一般
           </label>
           <label class="radio-inline">
             <input type="radio" name="star" id="star4" value="4"> 满意
           </label>
           <label class="radio-inline">
             <input type="radio" name="star" id="star5" value="5" checked> 很满意
           </label>
         </div>
        </fieldset>
      </div>
      <div class="form-row clearfix">
        <fieldset class="form-section form-section-active">
          <label class="input-label" for="judgeContent">评价内容</label>
          <textarea class="form-control" id="judgeContent" name="judgeContent" cols="100" rows="5"  placeholder="请输入评价内容"></textarea>
        </fieldset>
      </div>
    </form>
  </div>
  <div class="modal-ft"> <a id="J_cancelJudgeGoods" class="btn btn-gray" data-dismiss="modal" href="javascript: void(0);">取消</a> <a id="J_submitJudgeGoods" class="btn btn-primary" href="javascript: void(0);">提交</a> </div>
</div>


<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script> 
  <!-- 分页js --> 
  <script src="${ctxsta}/common/pager/jquery.pager.js"></script>
  <script type="text/javascript">
		var pagecount = $('#pager').attr('data-pager-totalPage'); // 总页面数
		var nowpage = $('#pager').attr('data-pager-nowpage'); // 当前页数
		var href = $('#pager').attr('data-pager-href'); // 链接地址
		$(document).ready(function() {
			$("#pager").pager({
				pagenumber : nowpage,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
		});
		PageClick = function(number) {
			$("#pager").pager({
				pagenumber : number,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
			window.location.href = href + number;
  	}
  </script>
  <
  <script >
    function judgeGoods(orderId,orderNumber,productNumber,productSpecNumber,productName){
        $("#orderId").val(orderId);
        $("#orderNumber").val(orderNumber);
        $("#productNumber").val(productNumber);
        $("#productSpecNumber").val(productSpecNumber);
        $("#goodName").val(productName);
        $("[name='star'][value='5']").prop("checked", "checked");
        $("#judgeContent").val("");
        $("#J_judgeGoodsDiv").show();
    }

    $(function() {

        $("#J_submitJudgeGoods").on("click", function() {
            var star = $('input[name="star"]:checked').val();
            var judgeContent=$("#judgeContent").val();
            var orderId=$("#orderId").val();
            var orderNumber=$("#orderNumber").val();
            var productNumber= $("#productNumber").val();
            var productSpecNumber=$("#productSpecNumber").val();

            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : baselocation + '/uc/order/judgeOrderProduct',
                data : {
                    "orderId":orderId,
                    "orderNumber":orderNumber,
                    "productNumber":productNumber,
                    "productSpecNumber":productSpecNumber,
                    "star":star,
                    "judgeContent":judgeContent,
                },
                success : function(result) {
                    if (result.code == 1) {
                        layer.alert("评价商品成功", {
                            icon : 1,closeBtn: 0
                        }, function(){
                            //window.location.href = baselocation + '/uc/order/list';
                            window.location.reload(); //刷新当前页面.
                        });
                    } else {
                        layer.alert(result.message, {
                            icon : 2
                        });
                    }
                }
            })
            $("#J_judgeGoodsDiv").hide();
        })
        $("#J_cancelJudgeGoods").on("click", function() {
            $("#J_judgeGoodsDiv").hide();

        })

        $(".close").on("click", function() {
            $("#J_judgeGoodsDiv").hide();
        })
    })

  </script>
</myfooter>
</body>
</html>