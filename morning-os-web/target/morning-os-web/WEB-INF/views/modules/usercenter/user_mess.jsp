<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>个人信息 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/address.css">
<link rel="stylesheet" href="${ctxsta}/os/area/css/select2.css" />
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-view-box">
      <div class="box-hd">
        <h1 class="title"><span class="text">个人信息</span></h1>
        <div class="more clearfix">
        </div>
      </div>
      <div class="box-bd">
        <%--<div class="portal-sub">--%>
          <%--<img class="avatar" src="${ctximg}/${userVO.picImg}" width="160" height="160" alt="${userVO.userName}"> </a>--%>
        <%--</div>--%>
        <div class="uc-order-item uc-order-item-finish">
          <div id="editAddr" class="order-detail-info">
            <table class="info-table">
              <tbody>
              <tr>
                <th>注册手机：</th>
                <td>${userVO.telephone}</td>
              </tr>
              <tr>
                <th>注册时间：</th>
                <td><fmt:formatDate value="${userVO.regeistTime}" pattern="yyyy年MM月dd日 HH:mm:ss" /></td>
              </tr>
              <tr>
                <th>积分：</th>
                <td>${userVO.score}</td>
              </tr>
              <tr>
                <th>昵称：</th>
                <td>${userVO.userName}</td>
              </tr>
              <tr>
                <th>真实姓名：</th>
                <td>${userVO.realName}</td>
              </tr>
              <tr>
                <th>性别：</th>
                <td>${userVO.sexStr}</td>
              </tr>
              <tr>
                <th>年龄：</th>
                <td>${userVO.ageStr}</td>
              </tr>
              <tr>
                <th>所在地：</th>
                <td>${userVO.place}</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
          <div class="more clearfix">
            <div class="actions">
              <a id="J_payOrder" class="btn btn-small btn-primary"  href="${ctx}/uc/user/addMess">完善个人信息</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>


<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script>
</myfooter>
</body>
</html>