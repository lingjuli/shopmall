<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>订单详情 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/address.css">
<link rel="stylesheet" href="${ctxsta}/os/area/css/select2.css" />
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-view-box">
      <div class="box-hd">
        <h1 class="title"><span class="text">服务单详情</span></h1>
        <div class="more clearfix">
          <h2 class="subtitle">服务单号：${returnVO.returnId} <span class="tag tag-subsidy"></span> </h2>
          <div class="actions">
          </div>
        </div>
      </div>
      <div class="box-bd">
        <div class="uc-order-item uc-order-item-finish">
          <div class="order-detail">
            <div class="order-summary">
              <div class="order-status">
                ${returnVO.returnStatusStr}
              </div>
            </div>
            <table class="order-items-table">
              <tbody>
                <c:forEach items="${returnVO.orderProducts}" var="orderProduct">
                  <tr>
                    <td class="col col-thumb"><div class="figure figure-thumb"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}" data-stat-id="${orderProduct.productNumber}"> <img src="${ctximg}/${orderProduct.picImg}" width="80" height="80" alt="${orderProduct.name}"> </a> </div></td>
                    <td class="col col-name"><p class="name"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}" data-stat-id="${orderProduct.productNumber}">${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}</a> </p></td>
                    <td class="col col-price"><p class="price">${orderProduct.price}元 × ${orderProduct.buyNumber}</p></td>
                    <td class="col col-actions">${orderProduct.productAmount}元</td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
          <div  class="order-detail-info">
            <h3>退货原因</h3>
            <table class="info-table">
              <tbody>
              <tr>
                <th>退货原因：</th>
                <td>${returnReasonStr}</td>
              </tr>
              <tr>
                <th>问题描述：</th>
                <td>${returnVO.questionRemark}</td>
              </tr>
              </tbody>
            </table>
            <div class="actions">
            </div>
          </div>
          <c:if test="${returnVO.returnStatus eq 2 || returnVO.returnStatus eq 3 || returnVO.returnStatus eq 4 || returnVO.returnStatus eq 5  }">
          <div  class="order-detail-info">
            <h3>寄送地址</h3>
            <table class="info-table">
              <tbody>
                <tr>
                  <th>收货人：</th>
                  <td>${returnVO.receiveMan}</td>
                </tr>
                <tr>
                  <th>联系电话：</th>
                  <td>${returnVO.receivePhone}</td>
                </tr>
                <tr>
                  <th>收货地址：</th>
                  <td>${returnVO.receiveAddress}</td>
                </tr>
              </tbody>
            </table>
            <div class="actions">
            </div>
          </div>
          <div  class="order-detail-info">
            <h3>寄送快递信息</h3>
            <table class="info-table">
              <tbody>
              <tr>
                <th>快递公司：</th>
                <td>${returnVO.logisticsCom}</td>
              </tr>
              <tr>
                <th>快递单号：</th>
                <td>${returnVO.logisticsNo}</td>
              </tr>
              </tbody>
            </table>
            <div class="actions">
            </div>
          </div>
          </c:if>
          <div  class="order-detail-info">
            <h3>操作时间</h3>
            <table class="info-table">
              <tbody>
              <tr>
                <th>申请时间：</th>
                <td> <fmt:formatDate value="${returnVO.createTime}" pattern="yyyy年MM月dd日 HH:mm" /></td>
              </tr>
              <tr>
                <th>审核时间：</th>
                <td><fmt:formatDate value="${returnVO.auditTime}" pattern="yyyy年MM月dd日 HH:mm" /></td>
              </tr>
              <c:if test="${returnVO.returnStatus eq 3 || returnVO.returnStatus eq 4 || returnVO.returnStatus eq 5 }">
              <tr>
                <th>收货时间：</th>
                <td><fmt:formatDate value="${returnVO.receiveTime}" pattern="yyyy年MM月dd日 HH:mm" /> </td>
              </tr>
              <tr>
                <th>退款时间：</th>
                <td><fmt:formatDate value="${returnVO.returnTime}" pattern="yyyy年MM月dd日 HH:mm" /> </td>
              </tr>
              </c:if>
              </tbody>
            </table>
            <div class="actions">
            </div>
          </div>
          <div class="order-detail-total">
            <table class="total-table">
              <tbody>
              <tr>
                <th>商品金额：</th>
                <td><span class="num">${productTotalAmt}</span>元</td>
              </tr>
              <c:if test="${returnVO.returnStatus eq 2 || returnVO.returnStatus eq 3 || returnVO.returnStatus eq 4 || returnVO.returnStatus eq 5  }">
                <tr>
                  <th>审核退款金额：</th>
                  <td><span class="num">${returnVO.returnAmount}</span>元</td>
                </tr>
              </c:if>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal-backdrop in" style="display: none; width: 100%; height: 1854px;"></div>
<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script>
</myfooter>
</body>
</html>