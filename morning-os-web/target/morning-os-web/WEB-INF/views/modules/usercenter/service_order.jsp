<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>评价晒单 -</title>
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-list-box">
      <div class="box-hd">
        <h1 class="title">服务单</h1>
        <div class="more clearfix">
        </div>
      </div>
      <div class="box-bd">
        <div id="J_orderList">
          <ul class="order-list">
            <c:forEach items="${returnVOs}" var="returnVO">
                <li class="uc-order-item uc-order-item-shipping">
                  <div class="order-detail">
                    <div class="order-summary">
                      <div class="order-status">${returnVO.returnStatusStr}</div>
                      <%--<p class="order-desc J_deliverDesc"> 评价商品获积分 </p>--%>
                    </div>
                    <table class="order-detail-table">
                      <thead>
                      <tr>
                        <th class="col-main"> <p class="caption-info">
                          <fmt:formatDate value="${returnVO.createTime}" pattern="yyyy年MM月dd日 HH:mm" />
                          <span class="sep">|</span>服务单号： ${returnVO.returnId} <%-- <a href="${ctx}/uc/order/${orderVO.orderNumber}">${orderVO.orderNumber}</a>--%></p>
                        </th>
                        <th class="col-sub">
                         &nbsp; <%--<p class="caption-price">订单金额：<span class="num">${orderVO.payAmount}</span>元</p>--%>
                        </th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td class="order-items"><ul class="goods-list">
                          <c:forEach items="${returnVO.orderProducts}" var="orderProduct">
                            <li>
                              <div class="figure figure-thumb"> <a href="${ctx}/detail/${orderProduct.productNumber}" target="_blank"> <img src="${ctximg}/${orderProduct.picImg}" width="80" height="80" alt="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}" title="${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}"> </a> </div>
                              <p class="name"> <a target="_blank" href="${ctx}/detail/${orderProduct.productNumber}">${orderProduct.name}&nbsp;&nbsp;${orderProduct.productSpecName}</a> </p>
                              <p class="price">${orderProduct.price}元 × ${orderProduct.buyNumber}</p>
                            </li>
                          </c:forEach>
                        </ul></td>
                        <td class="order-actions">
                          <c:if test="${returnVO.returnStatus eq 2}">
                             <a class="btn btn-small btn-primary" href="javascript:judgeGoods('${returnVO.returnId}');">填写物流信息</a>
                          </c:if>
                          <a class="btn btn-small btn-line-gray" href="${ctx}/uc/order/svcdetail/${returnVO.returnId}">服务单详情</a>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </li>

            </c:forEach>
          </ul>
        </div>
        <div id="J_orderListPages">
          <c:if test="${pageInfo.total gt pageInfo.limit and not empty returnVOs}">
            <div id="pager" data-pager-href="${ctx}/uc/order/servicelist?page=" data-pager-totalPage="${pageInfo.totalPage}" data-pager-nowpage="${pageInfo.current}" data-pager-total="${pageInfo.total}"></div>
          </c:if>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="J_judgeGoodsDiv" class="modal modal-verify modal-hide in disabled" aria-hidden="false" style=" border:1px solid #ff6700; display: none;" >
  <div class="modal-hd">
    <h3 class="title">填写物流信息</h3>
    <a class="close" data-dismiss="modal" href="javascript: void(0);"><i class="glyphicon glyphicon-remove"></i></a> </div>
  <div class="modal-bd">
    <form id="J_judgeForm" class="form-update-addr" method="post" action="">
      <div class="form-row clearfix" style="width: 100%">
        <fieldset class="form-section form-section-active">
          <label class="input-label" >服务单号</label>
          <textarea class="form-control" id="returnId" name="returnId" cols="100" rows="1" readonly ></textarea>
         </fieldset>
      </div>
      <div class="form-row clearfix">
        <fieldset class="form-section form-section-active">
          <label class="input-label" for="logisticsCom">快递公司</label>
          <br>
          <select id="logisticsCom" name="logisticsCom"   style="width: 200px">
            <option value="">--请选择--</option>
            <c:forEach items="${logisticsComList}" var="logisticsCom">
              <option value="${logisticsCom.status}"> ${logisticsCom.stateInfo}</option>
            </c:forEach>
          </select>
          <%--<textarea class="form-control" id="logisticsCom" name="logisticsCom" cols="100" rows="1"  placeholder="请输入快递公司"></textarea>--%>
        </fieldset>
      </div>
      <div class="form-row clearfix">
        <fieldset class="form-section form-section-active">
          <label class="input-label" for="logisticsNo">快递单号</label>
          <textarea class="form-control" id="logisticsNo" name="logisticsNo" cols="100" rows="1"  placeholder="请输入快递单号"></textarea>
        </fieldset>
      </div>
    </form>
  </div>
  <div class="modal-ft"> <a id="J_cancelJudgeGoods" class="btn btn-gray" data-dismiss="modal" href="javascript: void(0);">取消</a> <a id="J_submitJudgeGoods" class="btn btn-primary" href="javascript: void(0);">提交</a> </div>
</div>


<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script> 
  <!-- 分页js --> 
  <script src="${ctxsta}/common/pager/jquery.pager.js"></script>
  <script type="text/javascript">
		var pagecount = $('#pager').attr('data-pager-totalPage'); // 总页面数
		var nowpage = $('#pager').attr('data-pager-nowpage'); // 当前页数
		var href = $('#pager').attr('data-pager-href'); // 链接地址
		$(document).ready(function() {
			$("#pager").pager({
				pagenumber : nowpage,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
		});
		PageClick = function(number) {
			$("#pager").pager({
				pagenumber : number,
				pagecount : pagecount,
				buttonClickCallback : PageClick
			});
			window.location.href = href + number;
  	}
  </script>
  <
  <script >
      $("#logisticsCom").val(1);
    function judgeGoods(returnId){
        $("#returnId").val(returnId);
        $("#logisticsCom").val("");
        $("#logisticsNo").val("");
        $("#J_judgeGoodsDiv").show();
    }

    $(function() {

        $("#J_submitJudgeGoods").on("click", function() {

            var returnId=$("#returnId").val();
            var logisticsCom=$("#logisticsCom").val();
            var logisticsNo=$("#logisticsNo").val();
            if(logisticsCom==''){
                layer.alert("请选择快递公司");
                return;
            }
            if($.trim(logisticsNo)==''){
                layer.alert("请输入快递单号");
                return;
            }
            logisticsNo=$.trim(logisticsNo);
            // alert("服务单号："+returnId);
            // alert("快递公司："+logisticsCom);
            // alert("快递单号："+logisticsNo);
            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : baselocation + '/uc/order/commitServiceInfo',
                data : {
                    "returnId":returnId,
                    "logisticsCom":logisticsCom,
                    "logisticsNo":logisticsNo,
                },
                success : function(result) {
                    if (result.code == 1) {
                        layer.alert("物流信息提交成功", {
                            icon : 1,closeBtn: 0
                        }, function(){
                            //window.location.href = baselocation + '/uc/order/list';
                            window.location.reload(); //刷新当前页面.
                        });
                    } else {
                        layer.alert(result.message, {
                            icon : 2
                        });
                    }
                }
            })
            $("#J_judgeGoodsDiv").hide();
        })
        $("#J_cancelJudgeGoods").on("click", function() {
            $("#J_judgeGoodsDiv").hide();

        })

        $(".close").on("click", function() {
            $("#J_judgeGoodsDiv").hide();
        })
    })

  </script>
</myfooter>
</body>
</html>