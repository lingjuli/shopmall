<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/layouts/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>完善个人信息 -</title>
<link rel="stylesheet" href="${ctxsta}/os/css/address.css">
<link rel="stylesheet" href="${ctxsta}/os/area/css/select2.css" />
</head>
<body>
<div class="span16">
  <div class="uc-box uc-main-box">
    <div class="uc-content-box order-view-box">
      <div class="box-hd">
        <h1 class="title"><span class="text">完善个人信息</span></h1>
        <div class="more clearfix">
        </div>
      </div>
      <div class="box-bd">
        <div class="uc-order-item uc-order-item-finish">
          <div id="editAddr" class="order-detail-info">
            <table class="info-table">
              <tbody>
              <tr>
                <th>注册手机：</th>
                <td>${userVO.telephone}</td>
              </tr>
              <tr>
                <th>注册时间：</th>
                <td><fmt:formatDate value="${userVO.regeistTime}" pattern="yyyy年MM月dd日 HH:mm:ss" /></td>
              </tr>
              <tr>
                <th>积分：</th>
                <td>${userVO.score}</td>
              </tr>
              <tr>
                <th>昵称：</th>
                <td>
                    <input type="text" name="userName"  value="${userVO.userName}"  maxlength="20" class="form-control required "  id="userName" />
                </td>
              </tr>
              <tr>
                <th>真实姓名：</th>
                <td>
                    <input type="text" name="realName" maxlength="10" class="form-control required "  id="realName" value="${userVO.realName}" />
               </td>
              </tr>
              <tr>
                <th>性别：</th>
                <td>
                  <select  name="sex" id="sex"
                           class="form-control chosen-select" tabindex="2">
                    <option value="0">-请选择-</option>
                    <option value="1">男</option>
                    <option value="2">女</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th>年龄：</th>
                <td>
                  <input type="text" name="age" maxlength="10" class="form-control required "  id="age" value="${userVO.age}" />
                </td>
              </tr>
              <tr>
                <th>所在地：</th>
                <td>
                  <div class="form-row clearfix" id="J_modalVerify"   data-province_id="${userVO.provinceId}" data-province_name="${userVO.provinceName}" data-city_id="${userVO.cityId}" data-city_name="${userVO.cityName}" >
                    <div class="form-section form-section-province">
                      <select id="loc_province" class="select-1"></select>
                      <select id="loc_city" class="select-2"> </select>
                    </div>
                  </div>

                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="more clearfix">
          <div class="actions">
            <a id="J_payOrder" class="btn btn-small btn-primary"  href="javascript:saveMess();">提交</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<myfooter> 
  <!-- layer javascript --> 
  <script src="${ctxsta}/common/layer/layer.js"></script>
  <script src="${ctxsta}/os/area/js/area.js"></script>
  <script src="${ctxsta}/os/area/js/location.js"></script>
  <script src="${ctxsta}/os/area/js/select2.js"></script>
  <script src="${ctxsta}/os/area/js/select2_locale_zh-CN.js"></script>
  <script>
    $("#sex").val(${userVO.sex});
    showLocation();
    var b = $("#J_modalVerify"),
    province_id = b.attr("data-province_id"),
    province_name = b.attr("data-province_name"),
    city_id = b.attr("data-city_id"),
    city_name = b.attr("data-city_name");

    var loc = new Location();
    if(province_id!=0){
        $("#loc_province").find("option[value='" + province_id + "']").prop("selected", !0);
        $("#select2-chosen-1").html(province_name);
        loc.fillOption('loc_city', '0,' + province_id);
    }
    if(city_id!=0){
        $("#loc_city").find("option[value='" + city_id + "']").prop("selected", !0);
        $("#select2-chosen-2").html(city_name);
    }
    function saveMess(){
        var userName=$("#userName").val();
        var realName=$("#realName").val();
        var sex=$("#sex").val();
        var age=$("#age").val();
        if(!isNonEmpty(userName)){
            layer.alert("昵称不能为空", {icon : 2});
            $("#userName").focus();
            return;
        }
        if(!isUname(userName)){
            layer.alert("昵称为3-20位，以中文或字母开头，由中文、字母、数字或下划线等组成。", {icon : 2});
            $("#userName").focus();
            return;
        }
        if(!lengthBetween(userName,"3-20")){
            layer.alert("昵称长度为3-20位", {icon : 2});
            $("#userName").focus();
            return;
        }

        if(!isNonEmpty(realName)){
            layer.alert("真实姓名不能为空", {icon : 2});
            $("#realName").focus();
            return;
        }
        if(!isZh(realName)){
            layer.alert("真实姓名由汉字组成。", {icon : 2});
            $("#realName").focus();
            return;
        }
        if(!lengthBetween(realName,"2-10")){
            layer.alert("真实姓名长度为2-10位", {icon : 2});
            $("#realName").focus();
            return;
        }

        if(sex==0){
            layer.alert("请选择性别。", {icon : 2});
            $("#sex").focus();
            return;
        }
        if(age==0 || age.length<=0 ){
            layer.alert("请输入年龄。", {icon : 2});
            $("#age").focus();
            return;
        }
        if(!isInt(age)){
            layer.alert("请输入数字格式的年龄。", {icon : 2});
            $("#age").focus();
            return;
        }
        if(!numBetween(age,"1-100")){
            layer.alert("年龄范围为1-100之间", {icon : 2});
            $("#age").focus();
            return;
        }
        var sprovinceId = $("#loc_province").val();
        var scityId = $("#loc_city").val();
        //var stownId = $("#loc_town").val();
        var sprovinceName = $("#select2-chosen-1").html();
        var scityName = $("#select2-chosen-2").html();
        //var sdistrictName = $("#select2-chosen-3").html();
        if(!isNonEmpty(sprovinceId)){
            layer.alert("请选择省份", {icon : 2});
            $("#loc_province").focus();
            return;
        }
        if(!isNonEmpty(scityId)){
            layer.alert("请选择地级市", {icon : 2});
            $("#loc_city").focus();
            return;
        }


        $.ajax({
            type : "POST",
            url : baselocation + '/uc/user/saveMess',
            data : {
                "userName" : userName,
                "realName" : realName,
                "sex" : sex,
                "age" : age,
                "provinceId" : sprovinceId,
                "provinceName" : sprovinceName,
                "cityId" : scityId,
                "cityName" : scityName
            },
            dataType : "json",
            success : function(result) {
                if (result.code == 1) {
                    //layer.alert("保存成功", {icon : 1});
                    //window.location.href = baselocation + '/uc/user/userMess';
                    layer.alert("保存成功", {
                        icon : 1,closeBtn: 0
                    }, function(){
                        window.location.href = baselocation + '/uc/user/userMess';
                    });
                } else {
                    layer.alert("保存失败", {icon : 2});
                }
            }
        })

    }


    var rule = { //验证规则
            phone : /^1[2|3|4|5|6|7|8|9]\d{9}$/,
            mail : /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
            company : /^[\u4E00-\u9FA5a-zA-Z][\u4E00-\u9FA5a-zA-Z0-9\s-,-.]*$/,
            uname : /^[\u4E00-\u9FA5a-zA-Z][\u4E00-\u9FA5a-zA-Z0-9_]*$/,
            zh : /^[\u4e00-\u9fa5]+$/, //纯中文
            card : /^((1[1-5])|(2[1-3])|(3[1-7])|(4[1-6])|(5[0-4])|(6[1-5])|71|(8[12])|91)\d{4}(((((19|20)((\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(\d{2}(0[13578]|1[02])31)|(\d{2}02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[48])0229)))|20000229)\d{3}(\d|X|x))|(((\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(\d{2}(0[13578]|1[02])31)|(\d{2}02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[48])0229))\d{3}))$/, //身份证号
            int : /^[0-9]*$/,
            s : ''
    };

      function isNonEmpty(value) { //不能为空
        if (value.length) return true; else return false ;
      }
      function numBetween(value,range) { //大于小于
          var min = parseInt(range.split('-')[0]);
          var max = parseInt(range.split('-')[1]);
        if (value >= min && value <= max) return true; else return false ;
     }
    function lengthBetween(value,range) { //大于小于
        var min = parseInt(range.split('-')[0]);
        var max = parseInt(range.split('-')[1]);
        if (value.length >= min && value.length <= max) return true; else return false ;
    }
     function isInt(value) {
        if (rule.int.test(value)) return true; else return false ;
     }
      function isUname(value) {
        if (rule.uname.test(value)) return true; else return false ;
    }
    function isZh(value) {
        if (rule.zh.test(value)) return true; else return false ;
    }
  </script>

</myfooter>
</body>
</html>