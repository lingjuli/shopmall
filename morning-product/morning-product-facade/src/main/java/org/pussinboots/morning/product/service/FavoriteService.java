package org.pussinboots.morning.product.service;



import org.pussinboots.morning.product.entity.FavoriteDO;

import java.util.List;
import java.util.Map;

/**
 * 收藏夹表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-08 17:52:06
 */
public interface FavoriteService {
	
	FavoriteDO get(Long favoriteId);
	
	List<FavoriteDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(FavoriteDO favorite);
	
	int update(FavoriteDO favorite);
	
	int remove(Long favoriteId);
	
	int batchRemove(Long[] favoriteIds);
}
