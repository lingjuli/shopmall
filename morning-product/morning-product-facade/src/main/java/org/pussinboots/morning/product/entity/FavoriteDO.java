package org.pussinboots.morning.product.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



/**
 * 收藏夹表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-08 17:52:06
 */
public class FavoriteDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//收藏表ID
	private Long favoriteId;
	//用户ID
	private Long userId;
	//商品ID
	private Long productId;
	//商品编号
	private Long productNumber;
	//商品名称
	private String name;
	//展示图片
	private String picImg;
	//显示价格
	private BigDecimal showPrice;
	//商品状态：1,上架；2,下架
	private Integer status;
	//创建时间
	private Date createTime;
	//创建者
	private String createBy;
	//品牌ID
	private Long brandId;
	//创建者
	private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
	 * 设置：收藏表ID
	 */
	public void setFavoriteId(Long favoriteId) {
		this.favoriteId = favoriteId;
	}
	/**
	 * 获取：收藏表ID
	 */
	public Long getFavoriteId() {
		return favoriteId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：商品ID
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * 获取：商品ID
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * 设置：商品编号
	 */
	public void setProductNumber(Long productNumber) {
		this.productNumber = productNumber;
	}
	/**
	 * 获取：商品编号
	 */
	public Long getProductNumber() {
		return productNumber;
	}
	/**
	 * 设置：商品名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：商品名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：展示图片
	 */
	public void setPicImg(String picImg) {
		this.picImg = picImg;
	}
	/**
	 * 获取：展示图片
	 */
	public String getPicImg() {
		return picImg;
	}
	/**
	 * 设置：显示价格
	 */
	public void setShowPrice(BigDecimal showPrice) {
		this.showPrice = showPrice;
	}
	/**
	 * 获取：显示价格
	 */
	public BigDecimal getShowPrice() {
		return showPrice;
	}
	/**
	 * 设置：商品状态：1,上架；2,下架
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：商品状态：1,上架；2,下架
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建者
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建者
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：品牌ID
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	/**
	 * 获取：品牌ID
	 */
	public Long getBrandId() {
		return brandId;
	}
}
