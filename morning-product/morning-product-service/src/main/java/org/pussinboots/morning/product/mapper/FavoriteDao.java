package org.pussinboots.morning.product.mapper;



import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.pussinboots.morning.product.entity.FavoriteDO;

/**
 * 收藏夹表
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-08 17:52:06
 */
@Mapper
public interface FavoriteDao {

	FavoriteDO get(Long favoriteId);
	
	List<FavoriteDO> flist(Map<String, Object> map);
	
	int fcount(Map<String, Object> map);
	
	int save(FavoriteDO favorite);
	
	int update(FavoriteDO favorite);
	
	int remove(Long favorite_id);
	
	int batchRemove(Long[] favoriteIds);
}
