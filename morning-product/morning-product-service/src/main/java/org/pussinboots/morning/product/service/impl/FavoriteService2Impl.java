package org.pussinboots.morning.product.service.impl;

import org.pussinboots.morning.product.entity.FavoriteDO;
import org.pussinboots.morning.product.mapper.FavoriteDao;
import org.pussinboots.morning.product.service.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class FavoriteService2Impl implements FavoriteService {
	@Autowired
	private FavoriteDao favoriteDao;
	
	@Override
	public FavoriteDO get(Long favoriteId){
		return favoriteDao.get(favoriteId);
	}
	
	@Override
	public List<FavoriteDO> list(Map<String, Object> map){
		return favoriteDao.flist(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return favoriteDao.fcount(map);
	}
	
	@Override
	public int save(FavoriteDO favorite){
		return favoriteDao.save(favorite);
	}
	
	@Override
	public int update(FavoriteDO favorite){
		return favoriteDao.update(favorite);
	}
	
	@Override
	public int remove(Long favoriteId){
		return favoriteDao.remove(favoriteId);
	}
	
	@Override
	public int batchRemove(Long[] favoriteIds){
		return favoriteDao.batchRemove(favoriteIds);
	}
	
}
