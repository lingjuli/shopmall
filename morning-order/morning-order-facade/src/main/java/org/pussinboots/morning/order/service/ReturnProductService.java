package org.pussinboots.morning.order.service;

import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.order.entity.ReturnProductDO;
import org.pussinboots.morning.order.pojo.vo.ReturnProductVO;

import java.util.List;
import java.util.Map;

/**
 * 退货信息表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-28 10:23:00
 */
public interface ReturnProductService {

	BasePageDTO<ReturnProductVO> returnlist(Long userId, PageInfo pageInfo);

	ReturnProductVO   returnOne(Long returnId);
	
	ReturnProductDO get(Long returnId);
	
	List<ReturnProductDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ReturnProductDO returnProduct);
	
	int update(ReturnProductDO returnProduct);
	
	int remove(Long returnId);
	
	int batchRemove(Long[] returnIds);
}
