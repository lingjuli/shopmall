package org.pussinboots.morning.order.common.enums;

/**
 * 
* 项目名称：morning-order-facade   
* 类名称：OrderStatusEnum   
* 类描述：OrderStatusEnum 订单状态枚举表述常量数据字段      
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:40:21
*
 */
public enum ReturnProductEnum {

	SEVEN_DAYS(1, "7天无理由退货"),
	QUALITY_QUESTION(2, "质量不满意"),
	PRICE_QUESTION(3, "价格不满意"),
	DESIGN_QUESTION(4, "外观设计不满意"),
	;

	private Integer status;

	private String stateInfo;

	private ReturnProductEnum(Integer status, String stateInfo) {
			this.status = status;
			this.stateInfo = stateInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public static ReturnProductEnum stateOf(int index) {
		for (ReturnProductEnum statusEnum : values()) {
			if (statusEnum.getStatus() == index) {
				return statusEnum;
			}
		}
		return null;
	}
}
