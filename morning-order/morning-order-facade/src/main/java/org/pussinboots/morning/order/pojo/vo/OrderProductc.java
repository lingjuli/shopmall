package org.pussinboots.morning.order.pojo.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
* 项目名称：morning-order-facade   
* 类名称：OrderProduct   
* 类描述：OrderProduct / 订单明细表 实体类   
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:29:38
*
 */

public class OrderProductc extends Model<OrderProductc> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单商品ID
     */
	private Long orderProductId;
    /**
     * 订单ID
     */
	private Long orderId;
    /**
     * 商品编号
     */
	private Long productNumber;
    /**
     * 商品名称
     */
	private String name;
    /**
     * 展示图片
     */
	private String picImg;
    /**
     * 商品规格编号
     */
	private Long productSpecNumber;
    /**
     * 商品规格名称
     */
	private String productSpecName;
    /**
     * 价格
     */
	private BigDecimal price;
    /**
     * 积分
     */
	private Integer score;
    /**
     * 商品总数量
     */
	private Integer buyNumber;
    /**
     * 商品总积分
     */
	private Integer productScore;
    /**
     * 商品总金额
     */
	private BigDecimal productAmount;
    /**
     * 评论状态 0=未评论，1=已评论
     */
	private Integer commentStatus;

	/**
	 * 评论分数
	 */
	private Integer star;

	/**
	 * 评论内容
	 */
	private String content;

	/**
	 * 评论时间
	 */
	private Date judgetime;

	public Integer getStar() {
		return star;
	}

	public void setStar(Integer star) {
		this.star = star;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getJudgetime() {
		return judgetime;
	}

	public void setJudgetime(Date judgetime) {
		this.judgetime = judgetime;
	}

	public Long getOrderProductId() {
		return orderProductId;
	}

	public void setOrderProductId(Long orderProductId) {
		this.orderProductId = orderProductId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(Long productNumber) {
		this.productNumber = productNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicImg() {
		return picImg;
	}

	public void setPicImg(String picImg) {
		this.picImg = picImg;
	}

	public Long getProductSpecNumber() {
		return productSpecNumber;
	}

	public void setProductSpecNumber(Long productSpecNumber) {
		this.productSpecNumber = productSpecNumber;
	}

	public String getProductSpecName() {
		return productSpecName;
	}

	public void setProductSpecName(String productSpecName) {
		this.productSpecName = productSpecName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getBuyNumber() {
		return buyNumber;
	}

	public void setBuyNumber(Integer buyNumber) {
		this.buyNumber = buyNumber;
	}

	public Integer getProductScore() {
		return productScore;
	}

	public void setProductScore(Integer productScore) {
		this.productScore = productScore;
	}

	public BigDecimal getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(BigDecimal productAmount) {
		this.productAmount = productAmount;
	}

	public Integer getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(Integer commentStatus) {
		this.commentStatus = commentStatus;
	}

	@Override
	protected Serializable pkVal() {
		return this.orderProductId;
	}

}
