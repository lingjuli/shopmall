package org.pussinboots.morning.order.service;

import java.util.List;


import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.order.entity.Order;
import org.pussinboots.morning.order.entity.OrderShipment;
import org.pussinboots.morning.order.pojo.vo.OrderShoppingCartVO;
import org.pussinboots.morning.order.pojo.vo.OrderVO;

import com.baomidou.mybatisplus.service.IService;
import org.pussinboots.morning.order.pojo.vo.OrdercVO;

/**
 * 
* 项目名称：morning-order-facade   
* 类名称：IOrderService   
* 类描述：Order / 订单表 业务逻辑层接口      
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:31:08
*
 */
public interface IOrderService extends IService<Order> {
	
	/**
	 * 根据订单信息创建订单
	 * @param order 订单信息
	 * @param orderShipment 订单配送信息
	 * @param shoppingCartVOs 购物车商品列表 
	 * @param userId 用户ID
	 * @return
	 */
	Long insertOrder(Order order, OrderShipment orderShipment, List<OrderShoppingCartVO> shoppingCartVOs, Long userId);
	
	/**
	 * 根据用户ID、订单状态列表、分页信息、搜索内容查找订单列表
	 * @param userId 用户ID
	 * @param pageInfo 分页信息
	 * @param typeValue 订单状态列表
	 * @param search 搜索内容
	 * @return
	 */
	BasePageDTO<OrderVO> list(Long userId, PageInfo pageInfo, String typeValue, String search);

	BasePageDTO<OrdercVO> listc(Long userId, PageInfo pageInfo, String typeValue, String search);

	/**
	 * 查询待支付，待收货订单数
	 * @param userId
	 * @param typeValue
	 * @param search
	 * @return
	 */
	Integer getOrderCount(Long userId,  String typeValue, String search);

	/**
	 * 计算待评价商品数
	 * @param userId
	 * @return
	 */
	Integer  waitJudgeProductCount(Long userId);
	
	/**
	 * 根据用户ID、订单编号查找订单信息
	 * @param userId 用户ID
	 * @param orderNumber 订单编号
	 * @return
	 */
	OrderVO getOrder(Long userId, Long orderNumber);
	
	/**
	 * 根据订单编号,用户ID,订单状态查找订单信息
	 * @param orderNumber 订单编号
	 * @param userId 用户ID
	 * @param status 订单状态
	 * @return
	 */
	Order getByOrderNumber(Long orderNumber, Long userId, Integer status);


	/**
	 * 根据订单编号,用户ID,订单状态查找订单信息
	 * @param orderNumber 订单编号
	 * @return
	 */
	Order getByOrderNumber(Long orderNumber);


	/**
	 * 根据ID更新订单
	 * @param order
	 * @return
	 */
	Integer updateOrder(Order order);

	/**
	 * 支付订单
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Integer payOrder(Long orderNumber, Long userId);

	/**
	 * 配货订单
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Integer pickingOrder(Long orderNumber, Long userId);


	/**
	 * 配货订单
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Integer returnMoneyOrder(Long orderNumber, Long userId);



	/**
	 * 订单发货
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Integer sendOrder(Long orderNumber, Long userId);


	/**
	 * 确认收货
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Order inOrder(Long orderNumber, Long userId);

	/**
	 * 评价订单（当订单的全部商品已评价时，更改订单状态为6-已评价）
	 * @param orderNumber
	 * @param userId
	 * @return
	 */
	Integer judgeOrder(Long orderNumber, Long userId);

	
	/**
	 * 根据订单编号,用户ID取消订单
	 * @param orderNumber 订单编号
	 * @param userId 用户ID
	 * @return
	 */
	Integer updateCancelOrder(Long orderNumber, Long userId);
	
	/**
	 * 根据订单编号,用户ID,送货时间类型修改送货时间
	 * @param orderNumber  订单编号
	 * @param shipmentTime 送货时间
	 * @param userId 用户ID
	 * @return
	 */
	Integer updateShipmentTime(Long orderNumber, Integer shipmentTime, Long userId);
}
