package org.pussinboots.morning.order.service;


import org.pussinboots.morning.common.pay.alipay.PayCallbackParam;
import org.pussinboots.morning.order.entity.PayDO;
import org.pussinboots.morning.order.pojo.vo.OrderVO;

import java.util.List;
import java.util.Map;

/**
 * 订单支付表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-10-12 21:31:25
 */
public interface PayService {
	
	PayDO get(Long id);
	
	List<PayDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PayDO pay);
	
	int update(PayDO pay);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	String genPayTradeNo(long userId);

	String savePayRecord(OrderVO order, Long userId, int channelId);

	/**
	 * 生成支付提交页面html
	 *
	 * @param sParaTemp
	 * @param strMethod
	 * @param strButtonName
	 * @return
	 */
	String buildAlipayRequestForm(Map<String, String> sParaTemp, String strMethod, String strButtonName);

	/**
	 * 支付宝支付回调
	 *
	 * @param payCallbackParam
	 */
	Long payCallback(PayCallbackParam payCallbackParam);

	/**
	 * 微信支付回调
	 *
	 * @param xmlString
	 */
	PayDO wxCallBack(String xmlString);

    void clearPayRecord(Long orderNumber);
}
