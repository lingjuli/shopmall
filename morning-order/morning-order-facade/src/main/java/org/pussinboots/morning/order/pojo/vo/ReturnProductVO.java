package org.pussinboots.morning.order.pojo.vo;

import org.pussinboots.morning.order.entity.OrderProduct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * 退货信息表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-28 10:23:00
 */
public class ReturnProductVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//退货ID
	private Long returnId;
	//用户ID
	private Long userId;
	//用户名称
	private String userName;
	//用户手机
	private String telehpone;
	//所属订单ID
	private Long orderId;
	//所属订单编号
	private Long orderNumber;
	//退货产品ids，英文逗号隔开
	private String orderProductIds;
	//退货原因
	private Integer returnReason;
	//问题描述
	private String questionRemark;
	//申请时间
	private Date createTime;
	//退货状态
	private Integer returnStatus;
	//物流公司
	private String logisticsCom;
	//物流单号
	private String logisticsNo;
	//审核时间
	private Date auditTime;
	//审核备注
	private String auditRemark;
	//审核操作人
	private String auditBy;
	//审核退还金额
	private BigDecimal returnAmount;
	//收货点ID
	private Integer receiveId;
	//收货人
	private String receiveMan;
	//收货地址
	private String receiveAddress;
	//收货电话
	private String receivePhone;
	//收货操作人
	private String receiveBy;
	//收货时间
	private Date receiveTime;
	//收货备注
	private String receiveRemark;
	//退款操作人
	private String returnBy;
	//退款时间
	private Date returnTime;
	//退款备注
	private String returnRemark;

	private String returnStatusStr;

	public String getReturnStatusStr() {
		return returnStatusStr;
	}

	public void setReturnStatusStr(String returnStatusStr) {
		this.returnStatusStr = returnStatusStr;
	}

	public ReturnProductVO() {
	}

	/**
	 * 订单明细表
	 */
	private List<OrderProduct> orderProducts;

	public List<OrderProduct> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProduct> orderProducts) {
		this.orderProducts = orderProducts;
	}

	/**
	 * 设置：退货ID
	 */
	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}
	/**
	 * 获取：退货ID
	 */
	public Long getReturnId() {
		return returnId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户名称
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名称
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：用户手机
	 */
	public void setTelehpone(String telehpone) {
		this.telehpone = telehpone;
	}
	/**
	 * 获取：用户手机
	 */
	public String getTelehpone() {
		return telehpone;
	}
	/**
	 * 设置：所属订单ID
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：所属订单ID
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：所属订单编号
	 */
	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * 获取：所属订单编号
	 */
	public Long getOrderNumber() {
		return orderNumber;
	}
	/**
	 * 设置：退货产品ids，英文逗号隔开
	 */
	public void setOrderProductIds(String orderProductIds) {
		this.orderProductIds = orderProductIds;
	}
	/**
	 * 获取：退货产品ids，英文逗号隔开
	 */
	public String getOrderProductIds() {
		return orderProductIds;
	}
	/**
	 * 设置：退货原因
	 */
	public void setReturnReason(Integer returnReason) {
		this.returnReason = returnReason;
	}
	/**
	 * 获取：退货原因
	 */
	public Integer getReturnReason() {
		return returnReason;
	}
	/**
	 * 设置：问题描述
	 */
	public void setQuestionRemark(String questionRemark) {
		this.questionRemark = questionRemark;
	}
	/**
	 * 获取：问题描述
	 */
	public String getQuestionRemark() {
		return questionRemark;
	}
	/**
	 * 设置：申请时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：申请时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：退货状态
	 */
	public void setReturnStatus(Integer returnStatus) {
		this.returnStatus = returnStatus;
	}
	/**
	 * 获取：退货状态
	 */
	public Integer getReturnStatus() {
		return returnStatus;
	}
	/**
	 * 设置：物流公司
	 */
	public void setLogisticsCom(String logisticsCom) {
		this.logisticsCom = logisticsCom;
	}
	/**
	 * 获取：物流公司
	 */
	public String getLogisticsCom() {
		return logisticsCom;
	}
	/**
	 * 设置：物流单号
	 */
	public void setLogisticsNo(String logisticsNo) {
		this.logisticsNo = logisticsNo;
	}
	/**
	 * 获取：物流单号
	 */
	public String getLogisticsNo() {
		return logisticsNo;
	}
	/**
	 * 设置：审核时间
	 */
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}
	/**
	 * 获取：审核时间
	 */
	public Date getAuditTime() {
		return auditTime;
	}
	/**
	 * 设置：审核备注
	 */
	public void setAuditRemark(String auditRemark) {
		this.auditRemark = auditRemark;
	}
	/**
	 * 获取：审核备注
	 */
	public String getAuditRemark() {
		return auditRemark;
	}
	/**
	 * 设置：审核操作人
	 */
	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}
	/**
	 * 获取：审核操作人
	 */
	public String getAuditBy() {
		return auditBy;
	}
	/**
	 * 设置：审核退还金额
	 */
	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}
	/**
	 * 获取：审核退还金额
	 */
	public BigDecimal getReturnAmount() {
		return returnAmount;
	}
	/**
	 * 设置：收货点ID
	 */
	public void setReceiveId(Integer receiveId) {
		this.receiveId = receiveId;
	}
	/**
	 * 获取：收货点ID
	 */
	public Integer getReceiveId() {
		return receiveId;
	}
	/**
	 * 设置：收货人
	 */
	public void setReceiveMan(String receiveMan) {
		this.receiveMan = receiveMan;
	}
	/**
	 * 获取：收货人
	 */
	public String getReceiveMan() {
		return receiveMan;
	}
	/**
	 * 设置：收货地址
	 */
	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}
	/**
	 * 获取：收货地址
	 */
	public String getReceiveAddress() {
		return receiveAddress;
	}
	/**
	 * 设置：收货电话
	 */
	public void setReceivePhone(String receivePhone) {
		this.receivePhone = receivePhone;
	}
	/**
	 * 获取：收货电话
	 */
	public String getReceivePhone() {
		return receivePhone;
	}
	/**
	 * 设置：收货操作人
	 */
	public void setReceiveBy(String receiveBy) {
		this.receiveBy = receiveBy;
	}
	/**
	 * 获取：收货操作人
	 */
	public String getReceiveBy() {
		return receiveBy;
	}
	/**
	 * 设置：收货时间
	 */
	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}
	/**
	 * 获取：收货时间
	 */
	public Date getReceiveTime() {
		return receiveTime;
	}
	/**
	 * 设置：收货备注
	 */
	public void setReceiveRemark(String receiveRemark) {
		this.receiveRemark = receiveRemark;
	}
	/**
	 * 获取：收货备注
	 */
	public String getReceiveRemark() {
		return receiveRemark;
	}
	/**
	 * 设置：退款操作人
	 */
	public void setReturnBy(String returnBy) {
		this.returnBy = returnBy;
	}
	/**
	 * 获取：退款操作人
	 */
	public String getReturnBy() {
		return returnBy;
	}
	/**
	 * 设置：退款时间
	 */
	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}
	/**
	 * 获取：退款时间
	 */
	public Date getReturnTime() {
		return returnTime;
	}
	/**
	 * 设置：退款备注
	 */
	public void setReturnRemark(String returnRemark) {
		this.returnRemark = returnRemark;
	}
	/**
	 * 获取：退款备注
	 */
	public String getReturnRemark() {
		return returnRemark;
	}
}
