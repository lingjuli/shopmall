package org.pussinboots.morning.order.common.enums;

/**
 * 
* 项目名称：morning-order-facade   
* 类名称：OrderStatusEnum   
* 类描述：OrderStatusEnum 订单状态枚举表述常量数据字段      
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:40:21
*
 */
public enum ReturnStatusEnum {

	RETURN_APPR(1, "已申请退货"),
	RETURN_AGREE(2, "已同意退货"),
	SEND_GOODS(3, "退货已发出"),
	RETURN_GOODS(4, "已收到退货"),
	RETURN_MONEY(5, "已完成退款"),
	RETURN_REFUSE(6,"退货已拒绝"),;

	private Integer status;

	private String stateInfo;

	private ReturnStatusEnum(Integer status, String stateInfo) {
			this.status = status;
			this.stateInfo = stateInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public static ReturnStatusEnum stateOf(int index) {
		for (ReturnStatusEnum statusEnum : values()) {
			if (statusEnum.getStatus() == index) {
				return statusEnum;
			}
		}
		return null;
	}
}
