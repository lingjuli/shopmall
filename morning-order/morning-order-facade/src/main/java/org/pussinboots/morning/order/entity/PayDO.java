package org.pussinboots.morning.order.entity;

import java.io.Serializable;
import java.util.Date;



/**
 * 订单支付表
 * 
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-10-12 21:31:25
 */
public class PayDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//ID
	private Long id;
	//用户ID
	private Long userId;
	//订单ID
	private Long orderId;
	//订单编号
	private Long orderNumber;
	//支付渠道：支付宝1，微信2,  积分3
	private Integer channelId;
	//支付状态：初始0，成功支付1，取消2
	private Integer status;
	//支付流水号
	private String payTradeNo;
	//支付机构流水号
	private String outTradeNo;
	//请求金额，单位为分
	private Integer requestFee;
	//支付金额，单位为分
	private Integer payFee;
	//支付成功时间
	private Date payTime;
	//用户付款账号
	private String payAccount;
	//创建时间
	private Date createTime;

	public PayDO() {
	}

	/**
	 * 设置：ID
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：ID
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：订单ID
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单ID
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：订单编号
	 */
	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * 获取：订单编号
	 */
	public Long getOrderNumber() {
		return orderNumber;
	}
	/**
	 * 设置：支付渠道：支付宝1，微信2,  积分3
	 */
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：支付渠道：支付宝1，微信2,  积分3
	 */
	public Integer getChannelId() {
		return channelId;
	}
	/**
	 * 设置：支付状态：初始0，成功支付1，取消2
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：支付状态：初始0，成功支付1，取消2
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：支付流水号
	 */
	public void setPayTradeNo(String payTradeNo) {
		this.payTradeNo = payTradeNo;
	}
	/**
	 * 获取：支付流水号
	 */
	public String getPayTradeNo() {
		return payTradeNo;
	}
	/**
	 * 设置：支付机构流水号
	 */
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	/**
	 * 获取：支付机构流水号
	 */
	public String getOutTradeNo() {
		return outTradeNo;
	}
	/**
	 * 设置：请求金额，单位为分
	 */
	public void setRequestFee(Integer requestFee) {
		this.requestFee = requestFee;
	}
	/**
	 * 获取：请求金额，单位为分
	 */
	public Integer getRequestFee() {
		return requestFee;
	}
	/**
	 * 设置：支付金额，单位为分
	 */
	public void setPayFee(Integer payFee) {
		this.payFee = payFee;
	}
	/**
	 * 获取：支付金额，单位为分
	 */
	public Integer getPayFee() {
		return payFee;
	}
	/**
	 * 设置：支付成功时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付成功时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：用户付款账号
	 */
	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}
	/**
	 * 获取：用户付款账号
	 */
	public String getPayAccount() {
		return payAccount;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
