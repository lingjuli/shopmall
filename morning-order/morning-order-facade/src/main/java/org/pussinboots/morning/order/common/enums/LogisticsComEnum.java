package org.pussinboots.morning.order.common.enums;

/**
 * 
* 项目名称：morning-order-facade   
* 类名称：OrderStatusEnum   
* 类描述：OrderStatusEnum 订单状态枚举表述常量数据字段      
* 创建人：代远航
* 创建时间：2019年5月10日 上午10:40:21
*
 */
public enum LogisticsComEnum {

	SHUNFENG(1, "顺丰"),
	ZHONGTONG(2, "中通"),
	SHENTONG(3, "申通"),
	YUANTONG(4, "圆通"),
	SHUNDA(5,"顺达"),
	;

	private Integer status;

	private String stateInfo;

	private LogisticsComEnum(Integer status, String stateInfo) {
			this.status = status;
			this.stateInfo = stateInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public static LogisticsComEnum stateOf(int index) {
		for (LogisticsComEnum statusEnum : values()) {
			if (statusEnum.getStatus() == index) {
				return statusEnum;
			}
		}
		return null;
	}
}
