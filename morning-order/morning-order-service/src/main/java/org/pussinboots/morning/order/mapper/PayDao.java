package org.pussinboots.morning.order.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.pussinboots.morning.order.entity.PayDO;

/**
 * 订单支付表
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-10-12 21:31:25
 */
@Mapper
public interface PayDao {

	PayDO get(Long id);
	
	List<PayDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PayDO pay);
	
	int update(PayDO pay);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
