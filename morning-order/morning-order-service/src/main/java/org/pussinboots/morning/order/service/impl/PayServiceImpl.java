package org.pussinboots.morning.order.service.impl;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.pussinboots.morning.common.enums.PayStatusEnum;
import org.pussinboots.morning.common.pay.alipay.PayCallbackParam;
import org.pussinboots.morning.common.pay.alipay.AlipaySubmit;
import org.pussinboots.morning.common.pay.wxpay.WxPayUtil;
import org.pussinboots.morning.common.pay.wxpay.WxpayConfig;
import org.pussinboots.morning.common.util.DateUtils;
import org.pussinboots.morning.common.util.MoneyUtil;
import org.pussinboots.morning.common.util.RandomUtils;
import org.pussinboots.morning.order.entity.PayDO;
import org.pussinboots.morning.order.mapper.PayDao;
import org.pussinboots.morning.order.pojo.vo.OrderVO;
import org.pussinboots.morning.order.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;


@Service
public class PayServiceImpl implements PayService {
	@Autowired
	private PayDao payDao;
	
	@Override
	public PayDO get(Long id){
		return payDao.get(id);
	}
	
	@Override
	public List<PayDO> list(Map<String, Object> map){
		return payDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return payDao.count(map);
	}
	
	@Override
	public int save(PayDO pay){
		return payDao.save(pay);
	}
	
	@Override
	public int update(PayDO pay){
		return payDao.update(pay);
	}
	
	@Override
	public int remove(Long id){
		return payDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return payDao.batchRemove(ids);
	}

	@Override
	public String genPayTradeNo(long userId) {
		return DateUtils.dateToStr(new Date(), DateUtils.YMdhms_noSpli) + RandomUtils.getString(10) + userId;
	}

	@Override
	public String savePayRecord(OrderVO order, Long userId, int channelId){
		String  payTradeNo="";
		Map<String, Object> map=new HashMap<>();
		map.put("orderNumber",order.getOrderNumber());
		map.put("channelId",channelId);
		map.put("userId",userId);
		List<PayDO> paylist=list(map);
		if(paylist!=null && paylist.size()>0){
			payTradeNo=paylist.get(0).getPayTradeNo();
		}else{
			PayDO  pay=new PayDO();
			pay.setUserId(userId);
			pay.setOrderId(order.getOrderId());
			pay.setOrderNumber(order.getOrderNumber());
			pay.setChannelId(channelId);
			pay.setStatus(0);
			payTradeNo=genPayTradeNo(userId);
			pay.setPayTradeNo(payTradeNo);
			pay.setOutTradeNo("");
			pay.setRequestFee(order.getPayAmount().intValue());
			pay.setPayFee(0);
			pay.setPayAccount("");
			pay.setCreateTime(new Date());
			save(pay);
		}
		return payTradeNo;
	}

	@Override
	public String buildAlipayRequestForm(Map<String, String> sParaTemp, String strMethod, String strButtonName) {

		return AlipaySubmit.buildRequest(sParaTemp, strMethod, strButtonName);
	}

	@Override
	public Long payCallback(PayCallbackParam payCallbackParam) {

		//校验交易状态
		if (!"TRADE_SUCCESS".equals(payCallbackParam.getTradeStatus())) {
			return null;
		}
		String payTradeNo = payCallbackParam.getPayTradeNo();
		Map<String, Object> map=new HashMap<>();
		map.put("payTradeNo",payTradeNo);
		map.put("status",0);
		map.put("channelId",payCallbackParam.getChannelId());
		List<PayDO> paylist=list(map);
		if (paylist==null || paylist.size()<=0) {
			return null;
		}

		PayDO pay = paylist.get(0);
		pay.setStatus(1);
		pay.setPayTime(new Date());
		pay.setOutTradeNo(payCallbackParam.getOutTradeNo());
		pay.setPayAccount(payCallbackParam.getBuyerEmail());
		pay.setPayFee(Integer.parseInt(payCallbackParam.getTotalFee()));
		payDao.update(pay);

		Long orderNumber=pay.getOrderNumber();
		return orderNumber;


	}

	@Override
	public PayDO wxCallBack(String xmlString) {

		SAXReader saxReader = new SAXReader();
		Document dom = null;
		try {
			dom = saxReader.read(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
		} catch (DocumentException | UnsupportedEncodingException ignored) {
		}

		Element rootElement = dom.getRootElement();

		Map<String, Object> param = new TreeMap<>();
		for (Object e : rootElement.elements()) {
			Element e1 = (Element) e;
			if (!"sign".equals(e1.getQName().getName())) {
				param.put(e1.getQName().getName(), e1.getText());
			}
		}
		String respSign = rootElement.element("sign").getText();
		String payTradeNo = rootElement.element("out_trade_no").getText();
		String outTradeNo = rootElement.element("transaction_id").getText();
		String openId = rootElement.element("openid").getText();
		String sign = WxPayUtil.getSign(param, WxpayConfig.signKey);
		if (!sign.toUpperCase().equals(respSign)) {
			return null;
		}
		String resultCode = rootElement.element("result_code").getText();
		Integer totalFee = 0;
		int status;
		if ("SUCCESS".equals(resultCode)) {
			totalFee = Integer.parseInt(rootElement.element("total_fee").getText());
			status = PayStatusEnum.SUCCESS.getCode();
		} else {
			status = PayStatusEnum.FAILED.getCode();
		}

		Map<String, Object> map=new HashMap<>();
		map.put("payTradeNo",payTradeNo);
		map.put("status",0);
		map.put("channelId",2);
		List<PayDO> paylist=list(map);
		if (paylist==null || paylist.size()<=0) {
			return null;
		}

		PayDO pay = paylist.get(0);
		pay.setStatus(status);
		pay.setPayTime(new Date());
		pay.setOutTradeNo(outTradeNo);
		pay.setPayAccount(openId);
		pay.setPayFee(MoneyUtil.fenToYuan(totalFee));
		payDao.update(pay);

		//Long orderNumber=pay.getOrderNumber();
		return pay;
	}

	@Override
	public void clearPayRecord(Long orderNumber) {
		Map<String, Object> map=new HashMap<>();
		map.put("orderNumber",orderNumber);
		map.put("status",0);
		List<PayDO> paylist=list(map);
		List<Long> idlist=new ArrayList<>();
		if (paylist!=null && paylist.size()>0) {
			for(PayDO pay: paylist){
				idlist.add(pay.getId());
			}
		}
		for(Long id: idlist){
			payDao.remove(id);
		}
	}

}
