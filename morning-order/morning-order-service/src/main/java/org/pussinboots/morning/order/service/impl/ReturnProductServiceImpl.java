package org.pussinboots.morning.order.service.impl;

import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.common.util.MyBeanUtils;
import org.pussinboots.morning.order.entity.OrderProduct;
import org.pussinboots.morning.order.entity.ReturnProductDO;
import org.pussinboots.morning.order.mapper.OrderProductMapper;
import org.pussinboots.morning.order.mapper.ReturnProductDao;
import org.pussinboots.morning.order.pojo.vo.ReturnProductVO;
import org.pussinboots.morning.order.service.ReturnProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





@Service
public class ReturnProductServiceImpl implements ReturnProductService {
	@Autowired
	private ReturnProductDao returnProductDao;
	@Autowired
	private OrderProductMapper orderProductMapper;




	@Override
	public BasePageDTO<ReturnProductVO> returnlist(Long userId, PageInfo pageInfo){
		Map<String, Object> map=new HashMap<>();
		map.put("userId",userId);
		pageInfo.setTotal(returnProductDao.count(map));
		map.put("limit",pageInfo.getLimit());
		map.put("offset",pageInfo.getOffset());
		System.out.println("===========开始获取退货订单列表========");
		List<ReturnProductDO> dlist =returnProductDao.list(map);
		System.out.println("===========开始获取退货订单列表========"+dlist.size());
		List<ReturnProductVO> vlist=new ArrayList<>();
		for(ReturnProductDO d: dlist){
			ReturnProductVO v=new ReturnProductVO();
			MyBeanUtils.copyProperties(d,v);
            //获取退货产品
			List<Long> idlist=new ArrayList<>();
			String[] idarr=v.getOrderProductIds().split(",");
			for(String  id: idarr){
				idlist.add(Long.parseLong(id));
			}
			List<OrderProduct>  oplist=orderProductMapper.selectBatchIds(idlist);
			v.setOrderProducts(oplist);
			vlist.add(v);
		}
		return new BasePageDTO<ReturnProductVO>(pageInfo, vlist);
	}

	@Override
	public ReturnProductVO   returnOne(Long returnId){
		ReturnProductDO  d=returnProductDao.get(returnId);
		ReturnProductVO v=new ReturnProductVO();
		MyBeanUtils.copyProperties(d,v);
		//获取退货产品
		List<Long> idlist=new ArrayList<>();
		String[] idarr=v.getOrderProductIds().split(",");
		for(String  id: idarr){
			idlist.add(Long.parseLong(id));
		}
		List<OrderProduct>  oplist=orderProductMapper.selectBatchIds(idlist);
		v.setOrderProducts(oplist);
		return v;
	}
	
	@Override
	public ReturnProductDO get(Long returnId){
		return returnProductDao.get(returnId);
	}
	
	@Override
	public List<ReturnProductDO> list(Map<String, Object> map){
		return returnProductDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return returnProductDao.count(map);
	}
	
	@Override
	public int save(ReturnProductDO returnProduct){
		return returnProductDao.save(returnProduct);
	}
	
	@Override
	public int update(ReturnProductDO returnProduct){
		return returnProductDao.update(returnProduct);
	}
	
	@Override
	public int remove(Long returnId){
		return returnProductDao.remove(returnId);
	}
	
	@Override
	public int batchRemove(Long[] returnIds){
		return returnProductDao.batchRemove(returnIds);
	}
	
}
