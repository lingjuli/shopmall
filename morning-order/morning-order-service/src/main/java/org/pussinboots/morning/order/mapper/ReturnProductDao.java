package org.pussinboots.morning.order.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.pussinboots.morning.order.entity.ReturnProductDO;

/**
 * 退货信息表
 * @author sailing
 * @email sailing_1983@126.com
 * @date 2019-09-28 10:23:00
 */
@Mapper
public interface ReturnProductDao {

	ReturnProductDO get(Long returnId);
	
	List<ReturnProductDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ReturnProductDO returnProduct);
	
	int update(ReturnProductDO returnProduct);
	
	int remove(Long return_id);
	
	int batchRemove(Long[] returnIds);
}
