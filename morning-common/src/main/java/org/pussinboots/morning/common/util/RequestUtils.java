package org.pussinboots.morning.common.util;


import javax.servlet.http.HttpServletRequest;



/**
 *
 */
public class RequestUtils {


	/**
	 * 获取访问主域名
	 * @param req
	 * @return
	 */
	public static  String getMainDomain(HttpServletRequest req ){
		String path1 = req.getContextPath();
		String serverName=req.getServerName();
		if(serverName.startsWith("www.")){
			serverName=serverName.replaceAll("www.","");
		}
		String  basePath =serverName+":"+req.getServerPort()+path1;
		if(req.getServerPort()==80){
			basePath = serverName+path1;
		}
		return basePath;
	}


	/**
	 * 获取访问路径（无主域名）
	 * @param req
	 * @return
	 */
	public static  String getAccessUrl(HttpServletRequest req ){
		String accessurl=req.getServletPath() ;     //请求页面或其他地址
		if(req.getQueryString() != null){
			accessurl += "?" + req.getQueryString();
		}
		return accessurl;
	}




}
