package org.pussinboots.morning.common.pay.wxpay;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.pussinboots.morning.common.pay.alipay.MD5;
import org.pussinboots.morning.common.util.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 微信工具类
 */
public class WxPayUtil {

    private static Logger logger = LoggerFactory.getLogger(WxPayUtil.class);

    private WxPayUtil() {

    }

    /**
     * 微信预支付
     *
     * @param outTradeNo
     * @return
     */
    public static String wxPrepay(String outTradeNo, int totalFee, String body) {

        Map<String, Object> sortedParams = new TreeMap<String, Object>();
        sortedParams.put("appid", WxpayConfig.appId);
        sortedParams.put("mch_id", WxpayConfig.mchId);
        sortedParams.put("nonce_str", RandomUtils.getString(10));
        sortedParams.put("body", body);
        sortedParams.put("out_trade_no", outTradeNo);
        sortedParams.put("total_fee", totalFee + "");
        sortedParams.put("spbill_create_ip", "0.0.0.0");
		sortedParams.put("notify_url", WxpayConfig.notifyUrl);
        sortedParams.put("trade_type", WxpayConfig.tradeType);

        String sign = getSign(sortedParams, WxpayConfig.signKey);

        sortedParams.put("sign", sign);

        String parseXML = parse2XML(sortedParams);

        logger.info("wxPrepay, req=" + parseXML);

        String response = WxpayHttpUtil.postForXML("https://api.mch.weixin.qq.com/pay/unifiedorder", parseXML, null);
        logger.info("wxPrepay, resp=" + response);

        return parseFromXMLForPrepay(response);
    }

    public  static String  checkWeixinPayStatus(){
        Map<String, Object> sortedParams = new TreeMap<String, Object>();
        sortedParams.put("mch_id", WxpayConfig.mchId);
        sortedParams.put("nonce_str", RandomUtils.getString(10));
        String sign = getSign(sortedParams, WxpayConfig.signKey);
        sortedParams.put("sign", sign);
        String parseXML = parse2XML(sortedParams);
        System.out.println("得到的XML："+parseXML);
        String response = WxpayHttpUtil.postForXML("https://apitest.mch.weixin.qq.com/sandboxnew/pay/getsignkey", parseXML, null);
        return response;
    }

    /**
     * 生成xml
     *
     * @param sortedParams
     * @return
     */
    private static String parse2XML(Map<String, Object> sortedParams) {
        if (!(sortedParams instanceof TreeMap)) {
            Map<String, Object> temp = new TreeMap<String, Object>();
            temp.putAll(sortedParams);
            sortedParams = temp;
        }

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("xml");

        for (Entry<String, Object> entry : sortedParams.entrySet()) {
            String k = entry.getKey();
            Object v = entry.getValue();
            if (StringUtils.isBlank(k) || StringUtils.isBlank(v.toString())) {
                continue;
            }
            Element addElement = root.addElement(k);
            addElement.setText(v.toString());

        }

        return document.getRootElement().asXML();
    }

    /**
     * 解析预支付回复
     *
     * @param postForXML
     * @return
     */
    private static String parseFromXMLForPrepay(String postForXML) {

        if (postForXML == null) {
            logger.warn("response is null when parseFromXMLForPrepay");

            return null;
        }

        SAXReader saxReader = new SAXReader();
        Document resp = null;
        try {
            resp = saxReader.read(new ByteArrayInputStream(postForXML.getBytes("UTF-8")));
        } catch (DocumentException e) {
            logger.error("", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }

        Element rootElement = resp.getRootElement();
        String returnCode = rootElement.element("return_code").getText();

        if ("SUCCESS".equals(returnCode)) {
            String resultCode = rootElement.element("result_code").getText();
            if ("SUCCESS".equals(resultCode)) {
                return rootElement.element("code_url").getText();
            }
        }

        return null;
    }


    /**
     * 获取签名
     *
     * @param sortedParams
     * @param signKey
     * @return
     */
    public static String getSign(Map<String, Object> sortedParams, String signKey) {

        StringBuilder builder = new StringBuilder();

        if (!(sortedParams instanceof TreeMap)) {
            Map<String, Object> temp = new TreeMap<String, Object>();
            temp.putAll(sortedParams);
            sortedParams = temp;
        }
        for (Entry<String, Object> entry : sortedParams.entrySet()) {
            String k = entry.getKey();
            Object v = entry.getValue();
            if (StringUtils.isBlank(k) || StringUtils.isBlank(v.toString())) {
                continue;
            }
            if (builder.length() != 0) {
                builder.append("&");
            }
            builder.append(k);
            builder.append("=");
            builder.append(v);

        }

        builder.append("&key=");
        builder.append(signKey);

        String md5Digest = null;
        try {
            md5Digest = MD5.sign(builder.toString(), "", "UTF-8");
        } catch (Exception e) {
            logger.warn("get wxpay sign when md5Digest error", e);
        }
        return md5Digest;
    }


}
