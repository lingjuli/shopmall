package org.pussinboots.morning.common.pay.wxpay;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 二维码生成工具
 * Created by Administrator on 2016/4/26.
 */
public class QRCodeUtil {
    public static final String QRCODE_DEFAULT_CHARSET = "UTF-8";

    public static final int QRCODE_DEFAULT_HEIGHT = 250;

    public static final int QRCODE_DEFAULT_WIDTH = 250;

    public static final String QRCODE_DEFAULT_FORMAT = "png";

    private static final int BLACK = 0xFF000000;
    private static final int WHITE = 0xFFFFFFFF;


    public static BufferedImage genQRCode(String msg) {
        return genQRCode(msg, null);
    }

    public static BufferedImage genQRCode(String msg, Integer margin) {
        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, QRCODE_DEFAULT_CHARSET);
        if (margin != null) {
            hints.put(EncodeHintType.MARGIN, margin);
        }
        BitMatrix bitMatrix = null;// 生成矩阵
        try {
            bitMatrix = new MultiFormatWriter().encode(msg,
                    BarcodeFormat.QR_CODE, QRCODE_DEFAULT_WIDTH, QRCODE_DEFAULT_HEIGHT, hints);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        assert bitMatrix != null;
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    public static BufferedImage genQRCodeWithLogo(String data, File logoFile) {
        try {
            BufferedImage qrcode = genQRCode(data, 1);
            BufferedImage logo = ImageIO.read(logoFile);
            int deltaWidth = QRCODE_DEFAULT_WIDTH - logo.getWidth();
            int deltaHeight = QRCODE_DEFAULT_HEIGHT - logo.getHeight();

            BufferedImage combined = new BufferedImage(QRCODE_DEFAULT_WIDTH, QRCODE_DEFAULT_HEIGHT, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) combined.getGraphics();
            g.drawImage(qrcode, 0, 0, null);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
            g.drawImage(logo, Math.round(deltaWidth / 2), Math.round(deltaHeight / 2), null);

            return combined;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
