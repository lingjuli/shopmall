package org.pussinboots.morning.common.util;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2016/4/5.
 */
public class MoneyUtil {

    /**
     * 元转换为分
     *
     * @param yuan
     * @return
     */
    public static int yuanToFen(String yuan) {
        BigDecimal b = new BigDecimal(yuan);
        BigDecimal ret = b.multiply(new BigDecimal(100));
        return ret.intValue();

    }

    /**
     * 分转换为元
     *
     * @param fen
     * @return
     */
    public static int fenToYuan(int fen) {
        BigDecimal b = new BigDecimal(fen);
        BigDecimal ret = b.divide(new BigDecimal(100));
        return ret.intValue();

    }

    public static void main(String arg[]) {
        System.out.println(MoneyUtil.fenToYuan(1));

        System.out.print(MoneyUtil.yuanToFen("1"));
    }
}
