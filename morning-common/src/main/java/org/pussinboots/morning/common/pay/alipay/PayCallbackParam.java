package org.pussinboots.morning.common.pay.alipay;


public class PayCallbackParam  {
    private String payTradeNo;
    private String outTradeNo;
    private String buyerEmail;
    private String tradeStatus;
    private String totalFee;
    private int channelId;
    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getPayTradeNo() {
        return payTradeNo;
    }

    public void setPayTradeNo(String payTradeNo) {
        this.payTradeNo = payTradeNo;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public int getChannelId() {
        return channelId;
    }
}
