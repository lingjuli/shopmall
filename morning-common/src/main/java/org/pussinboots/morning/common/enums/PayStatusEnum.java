package org.pussinboots.morning.common.enums;

/**
 * Created by Administrator on 2016/4/5.
 */
public enum PayStatusEnum {
    INIT(0, "初始化"),
    SUCCESS(1, "成功"),
    FAILED(2, "失败");

    private PayStatusEnum(final int code, final String desc) {
        this.code = code;
        this.desc = desc;
    }


    /**
     * 枚举码
     */
    private int code;

    /**
     * 枚举描述
     */
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
