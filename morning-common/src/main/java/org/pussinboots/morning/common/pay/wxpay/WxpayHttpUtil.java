package org.pussinboots.morning.common.pay.wxpay;


import org.apache.http.HttpEntity;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.BasicHttpEntity;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * http工具类
 *
 * @author fangyangtong
 * @version $Id: WxpayHttpUtil.java, v 0.1 2016年4月27日 下午5:37:09 fangyangtong Exp $
 */
public class WxpayHttpUtil {
    private static final int DEFAULT_MAX_CONNECTIONS = 100;
    private static final int DEFAULT_SO_TIMEOUT = 4000;

    private static final HttpClient HTTP_CLIENT;

    static {

        PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
        connectionManager.setMaxTotal(DEFAULT_MAX_CONNECTIONS);
        connectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTIONS);
        HTTP_CLIENT = new DefaultHttpClient(connectionManager);
        HTTP_CLIENT.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
        HTTP_CLIENT.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, DEFAULT_SO_TIMEOUT);

    }

    /**
     * post数据请求
     *
     * @param url
     * @param xml
     * @param headers
     * @return
     */
    public static String postForXML(String url, String xml, Map<String, String> headers) {

        HttpPost post = new HttpPost(url);
        String response = null;
        if (xml != null && !xml.isEmpty()) {
            if (headers != null) {
                for (Entry<String, String> e : headers.entrySet()) {
                    post.addHeader(e.getKey(), e.getValue());
                }
            }
            try {
                BasicHttpEntity requestBody = new BasicHttpEntity();
                requestBody.setContent(new ByteArrayInputStream(xml.getBytes("UTF-8")));
                requestBody.setContentLength(xml.getBytes("UTF-8").length);
                post.setEntity(requestBody);
                post.getParams().setParameter("http.protocol.cookie-policy", CookiePolicy.BROWSER_COMPATIBILITY);
            } catch (UnsupportedEncodingException e) {

            }
        }

        try {
            HttpEntity entity = HTTP_CLIENT.execute(post).getEntity();
            response = EntityUtils.toString(entity, "UTF-8");
            EntityUtils.consume(entity);
        } catch (IOException e) {

        } finally {

            post.releaseConnection();
        }

        return response;
    }


}
